var $captchaUrl = $('a[href="#reload-captcha"] img').attr('src');

$('a[href="#reload-captcha"]').click(function(e) {
    e.preventDefault();
    $(this).children('img').attr('src', $captchaUrl + '?id=' + Math.random());
    return false;
});
$(function() {
    $('#topNav ul>li:has(ul)').hover(function() {
        $(this).children('ul').slideDown();
    }, function() {
        $('#topNav ul>li ul').hide();    
    });
})
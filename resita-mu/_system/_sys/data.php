<?php

function sql_connect($host, $user, $password, $database) {
    $muweb_sql_connect = mssql_connect($host, $user, $password);
    $muweb_dbselect = mssql_select_db($database, $muweb_sql_connect);
}

function clean_var($var = NULL) {
    $newvar = @preg_replace('/[^a-zA-Z0-9\_\-\.]/', '', $var);
    if (@preg_match('/[^a-zA-Z0-9\_\-\.]/', $var)) {
        
    }
    return $newvar;
}

function clean_inject($var) {
    $var = str_replace("'", "", $var);
    $var = str_replace(";", "", $var);
    $var = str_replace("1=1", "", $var);
    $var = str_replace("1 = 1", "", $var);
    $var = str_replace("Table", "", $var);
    $var = str_replace("Drop", "", $var);
    $var = str_replace("DROP", "", $var);
    $var = str_replace("drop", "", $var);
    $var = str_replace("DELETE", "", $var);
    $var = str_replace("Delete", "", $var);
    $var = str_replace("delete", "", $var);
    return $var;
}

function error($text) {
    $error = '<span class="warning">' . $text . '</span>';
    return "$error";
}

?>
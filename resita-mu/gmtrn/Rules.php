<pre><p align="left" style="margin-top: 20; margin-left: 20; font-family:Arial, Helvetica, sans-serif; font-size:11px">
Regulament

1.NU divulgati nimanui linkul de la GM Panel.
2.NU divulgati nimanui parola de la contul de GM
3.NU abuzati de functia de GM.
4.NU folositi functia de GM in scopuri personale si pentru razbunare.
5.NU acordati disconnect fara motiv.
6.NU acordati ban fara motiv sau fara dovada.
7.NU exilati plaieri fara motiv.
8.NU teleportati pe nimeni fara motiv.
9.Ori ce ban/exile la expirarea termenului va trebui scos de GM care la si 
acordat.
10.In cazul in care trebuie sa dovediti motivul banului folositi 
www.fanhosting.info pentru a uploada dovezile.

In cazul in care nu veti respecta regulamentul General al GM veti fi 
pedepsiti conform regulamentului.
Ne Rezervam dreptul de a modifica regulamentul ori de cate ori este necesar.

Rules

1.Do not give to nobody the link of GM Panel.
2.Do not give to nobody your password of GM Account.
3.Do not abuse with the function of GM.
4.Do not use your GM function in personal purpose or to revenge.
5.Do not disconnect players without a reason.
6.Do not ban players without a reason or without a proof.
7.Do not exile players without a reason.
8.Do not teleport players without a reason.
9.Any Ban or Exile at expire period of ban need to be removed by the 
GM ho add the ban.
10.In case you need to proof the reason of ban use 
www.fanhosting.info too upload the proofs.

In case if you do not respect the rules of general GM you will 
be punished conform of rules.
We Reserv all Rights to change any time the rules any time need.

GM Commands

1. /gg - This command allow gm to write on global post
2. /status - This command allow gm to see the character status
3. /reload - This command allow gm to reload all server configuration
4. /drop - This command allow gm to make full items
5. /pkset - This command allow gm to setpk
6. /setzen - This command allow gm to set ammount of zen in his account.
7. /ban - This command allow gm to ban a player account.
8. ! - This command allow gm to write on global post.
9. /Disconnect - This command allow gm to disconnect players.
10. /move - This command allow gm to move players at fixed coordonates.
11. /trace - This command allow gm to trace other players

Attention Some command are disabled By Administrator , All Are 
Functional But disabled.
</p>
</pre></font>
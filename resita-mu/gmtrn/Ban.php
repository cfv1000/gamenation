<p align="center">
    <?php
    if (isset($_POST["ban_character"])) {
        ban_character($_SESSION['gm_name'], $_POST['character'], date('Y-m-d'), $_POST['ban_period'], $_POST['reason']);
    }

    if (isset($_POST["unban_character"])) {
        unban_character($_SESSION['gm_name'], $_POST['character'], date('Y-m-d'), $_POST['fn']);
    }
    ?>

</p>
<form action="" method="post" enctype="multipart/form-data" name="ban_character" id="ban_character">
    <table align="center" width="100%" border="0" cellspacing="4" cellpadding="0">
        <tr>
            <td width="136" class="normal_text"><div align="right" class="normal_text">Ban Given By</div></td>
            <td width="205" class="normal_text"><?php echo($_SESSION['gm_name']); ?></td>
        </tr>
        <tr>
            <td class="normal_text"><div align="right" class="normal_text">Character</div></td>
            <td class="normal_text"><?php
    if ($character_display_type == 1) {
        echo '<input name="character" class="buttons" type="text" id="character" size="15" maxlength="10">';
    }
    if ($character_display_type == 0) {
        characters();
    }
    ?>
            </td>
        </tr>
        <tr>
            <td class="normal_text"><div align="right" class="normal_text">Ban Date </div></td>
            <td class="normal_text"><? echo date('Y-m-d'); ?></td>
        </tr>
        <tr>
            <td class="normal_text"><div align="right" class="normal_text">Ban Period</div></td>
            <td class="but"><select name="ban_period" class="buttons" id="ban_peryod">
                    <option value="1">1 Day</option>
                    <option value="2">2 Days</option>
                    <option value="3">3 Days</option>
                    <option value="4">4 Days</option>
                    <option value="5">5 Days</option>
                    <option value="6">6 Days</option>
                    <option value="7">7 Days</option>
                    <option value="8">8 Days</option>
                    <option value="9">9 Days</option>
                    <option value="10">10 Days</option>
                    <option value="11">11 Days</option>
                    <option value="12">12 Days</option>
                    <option value="13">13 Days</option>
                    <option value="14">14 Days</option>
                    <option value="15">15 Days</option>
                    <option value="16">16 Days</option>
                    <option value="17">17 Days</option>
                    <option value="18">18 Days</option>
                    <option value="19">19 Days</option>
                    <option value="20">20 Days</option>
                    <option value="21">21 Days</option>
                    <option value="22">22 Days</option>
                    <option value="23">23 Days</option>
                    <option value="24">24 Days</option>
                    <option value="25">25 Days</option>
                    <option value="26">26 Days</option>
                    <option value="27">27 Days</option>
                    <option value="28">28 Days</option>
                    <option value="29">29 Days</option>
                    <option value="30">30 Days</option>
                    <option value="99">Permanent</option>
                </select></td>
        </tr>
        <tr>
            <td class="normal_text"><div align="right" class="normal_text">Print Screen </div></td>
            <td class="normal_text"><input name="userfile" class="buttons" type="file" id="userfile"></td>
        </tr>
        <tr>
            <td class="normal_text"><div align="right" class="normal_text">Reason</div></td>
            <td rowspan="2" class="normal_text"><textarea name="reason" class="terms" cols="30" rows="10" id="reason"></textarea></td>
        </tr>
        <tr>
            <td class="normal_text"><input name="ban_character" type="hidden" id="ban_character" value="ban_character"></td>
        </tr>

        <tr>
            <td colspan="2" align="center">
                <input type="submit" name="Submit" class="statsMain" value="Ban Character">
                <input type="reset" name="Reset" class="statsMain" value="Reset"></td>
        </tr></table>

</form>

<?php require_once("Banlist.php"); ?>
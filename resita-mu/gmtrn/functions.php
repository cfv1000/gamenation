<?php

if (preg_match("includes/functions.php", $_SERVER['SCRIPT_NAME'])) {
    die("Access Denied!");
}

function ban_character($baned_by, $character, $date, $ban_period, $reason, $screen) {
    $baned_by = str_replace("'", "", $baned_by);
    $baned_by = str_replace(";", "", $baned_by);

    $character = str_replace("'", "", $character);
    $character = str_replace(";", "", $character);

    $date = str_replace("'", "", $date);
    $date = str_replace(";", "", $date);


    $ban_period = str_replace("'", "", $ban_period);
    $ban_period = str_replace(";", "", $ban_period);

    $reason = str_replace("'", "", $reason);
    $reason = str_replace(";", "", $reason);

    $MAX_SIZE = 2000000;
    $FILE_MIMES = array('image/jpeg', 'image/jpg');
    $FILE_EXTS = array('.jpg');

    $target_path = $target_path . basename($_FILES['userfile']['name']);
    $_FILES['userfile']['tmp_name'];
    $target_path = "images/screens/";
    $file_type = $_FILES['userfile']['type'];



    $file_name = $character . " - " . $date . "(" . $ban_period . ").jpg";
    $target_path = $target_path . basename($file_name);

    $character_check = mssql_query("SELECT name FROM character WHERE name='$character'");
    $character_check_ = mssql_num_rows($character_check);

    $banlist_check = mssql_query("SELECT name FROM banlist WHERE name='$character'");
    $banlist_check_ = mssql_num_rows($banlist_check);

    $ban_check = mssql_query("SELECT ctlcode FROM character WHERE name='$character'");
    $ban_check_ = mssql_fetch_row($ban_check);


    if (empty($baned_by) || empty($character) || empty($date) || empty($ban_period) || empty($reason) || empty($file_name)) {
        echo "<img src='images/warning.gif'> Error: Some Fields Were Left Blank! <br><span class='link_logs'><a href='javascript:history.go(-1)'>Go Back.</a></span>";
    } elseif (!in_array($file_type, $FILE_MIMES) && !in_array($file_ext, $FILE_EXTS)) {
        echo "<img src='images/warning.gif'> Only .jpg files are allowed!<b>";
    } elseif (file_exists($filenamecheck)) {
        echo "<img src='images/warning.gif'> Image $file_name is already uploaded, please change file name!<b>";
    } elseif ($_FILES['userfile']['size'] > $MAX_SIZE) {
        echo "<img src='images/warning.gif'> Image $file_name has more then 2MB!<b>";
    } elseif ($character_check_ <= 0) {
        echo "<img src='images/warning.gif'> Error: Character $character Dose Not Exist! <br><span class='link_logs'><a href='javascript:history.go(-1)'>Go Back.</a></span>";
    } elseif ($ban_check_[0] == '1') {
        echo "<img src='images/warning.gif'> Error: Character $character Is Already Baned! <br><span class='link_logs'><a href='javascript:history.go(-1)'>Go Back.</a></span>";
    } elseif ($banlist_check_ > 0) {
        echo "<img src='images/warning.gif'> Error: Character $character Is Alread Added To Ban List! <br><span class='link_logs'><a href='javascript:history.go(-1)'>Go Back.</a></span>";
    } else {
        $time = time() + 60 * 60 * 24 * $ban_period;
        $date1 = date('Y-m-d', $time);

        if ($ban_period == 99) {
            $banlist_add = mssql_query("INSERT INTO banlist(name,baned_by,ban_period,date,screen, expires)VALUES('$character','$baned_by','Permanent','$date','$file_name','-')");
            $set_ban = mssql_query("Update character set [ctlcode]='1' where name='$character'");
        } else {

            $banlist_add = mssql_query("INSERT INTO banlist(name,baned_by,ban_period,date,screen, expires)VALUES('$character','$baned_by','$ban_period','$date','$file_name','$date1')");
            $set_ban = mssql_query("Update character set [ctlcode]='1' where name='$character'");
        }




        if (move_uploaded_file($_FILES['userfile']['tmp_name'], $target_path)) {
            echo "";
        }



        $logfile = 'gmtrn/logs/Ban.gmtrn';
        $ip = $_SERVER['REMOTE_ADDR'];
        $date2 = date('d-m-Y H:i:s');
        $data = "<font color=#FF0000>New Ban Set</font>\nBaned By: $baned_by\nBan Period: $ban_period\nCharacter Baned: <font color=#FF0000>$character</font>\nReason: $reason\nDate: $date2\n<font color=#FF0000>=========================================================</font>\n";

        $fp = fopen($logfile, 'a');
        fputs($fp, $data);
        fclose($fp);

        echo "<img src='images/ok.gif'>Character $character SuccesFully Baned!";
    }
}

function unban_character($unbaned_by, $character, $date, $fn) {

    $fn = $_POST['fn'];
    $unbaned_by = str_replace("'", "", $unbaned_by);
    $unbaned_by = str_replace(";", "", $unbaned_by);

    $character = str_replace("'", "", $character);
    $character = str_replace(";", "", $character);


    $character_check = mssql_query("SELECT name FROM character WHERE name='$character'");
    $character_check_ = mssql_num_rows($character_check);

    $banlist_check = mssql_query("SELECT name FROM banlist WHERE name='$character'");
    $banlist_check_ = mssql_num_rows($banlist_check);

    $ban_check = mssql_query("SELECT ctlcode FROM character WHERE name='$character'");
    $ban_check_ = mssql_fetch_row($ban_check);

    $banlist_remove = mssql_query("delete from banlist where name='$character'");
    $set_unban = mssql_query("Update character set [ctlcode]='0' where name='$character'");

    $logfile = 'gmtrn/logs/Unban.gmtrn';
    $ip = $_SERVER['REMOTE_ADDR'];
    $date2 = date('d-m-Y H:i:s');
    $data = "<font color=#FF0000>New UnBan Set</font>\nUnBaned By: $unbaned_by\nCharacter UnBaned: <font color=blue>$character</font>\nDate: $date2\n<font color=#FF0000>=========================================================</font>\n";
    $fp = fopen($logfile, 'a');
    fputs($fp, $data);
    fclose($fp);
    echo "<img src='images/ok.gif'>Character $character SuccesFully UnBaned!";
    $file_name = $_FILES['userfile']['name'];
    unlink("images/screens/$fn");
}

function characters() {
    $run = mssql_query("Select Name from Character");
    $count = mssql_num_rows($run);
    echo "<select class='buttons' name='character'>";
    for ($i = 0; $i < $count; ++$i) {
        $get = mssql_fetch_row($run);
        echo "<option value=" . $get[0] . ">" . $get[0] . "</option>";
    }
    echo "</select>";
}

function class_select($op) {
    echo "<select class='buttons' name='class'>";
    echo "<option value='0'>Dark Wizard</option>";
    echo "<option value='1'>Soul Master</option>";
    if ($op == 0) {
        echo "<option value='2'>Grand  Master</option>";
    }
    if ($op == 1) {
        echo "<option value='3'>Grand  Master</option>";
    }

    echo "<option value='16'>Dark Knight</option>";
    echo "<option value='17'>Blade Knight</option>";
    if ($op == 0) {
        echo "<option value='18'>Blade Master</option>";
    }
    if ($op == 1) {
        echo "<option value='19'>Blade Master</option>";
    }

    echo "<option value='32'>Elf</option>";
    echo "<option value='33'>Muse Elf</option>";
    if ($op == 0) {
        echo "<option value='34'>High Elf</option>";
    }
    if ($op == 1) {
        echo "<option value='35'>High Elf</option>";
    }

    echo "<option value='48'>Magic Gladiator</option>";
    if ($op == 0) {
        echo "<option value='49'>Duel Master</option>";
    }
    if ($op == 1) {
        echo "<option value='50'>Duel Master</option>";
    }

    echo "<option value='64'>Dark Lord</option>";
    if ($op == 0) {
        echo "<option value='65'>Lord Emperor</option>";
    }
    if ($op == 1) {
        echo "<option value='66'>Lord Emperor</option>";
    }

    echo "<option value='80'>Summoner</option>";
    echo "<option value='81'>Bloody Summoner</option>";
    if ($op == 0) {
        echo "<option value='82'>Dimension Master</option>";
    }
    if ($op == 1) {
        echo "<option value='83'>Dimension Master</option>";
    }
    echo "<option value='96'>Rage Fighter</option>";
    echo "<option value='98'>First Fighter</option>";
    echo "</select>";
}

function change_class($by, $char, $to, $date, $skill_tree_table_name) {
    $char = str_replace(";", "", $char);
    $char = str_replace("'", "", $char);
    $char = str_replace('"', '', $char);
    $from = mssql_query("SELECT Class from Character where Name='" . $char . "'");
    $check_char = mssql_num_rows($from);
    $from1 = mssql_fetch_array($from);
    $logfile = 'gmtrn/logs/ClassChange.gmtrn';
    $from2 = $from1['Class'];
    $mod = array(
        0 => 'Dark Wizard',
        1 => 'Soul Master',
        2 => 'Grand  Master',
        3 => 'Grand  Master', //secondary string 
        16 => 'Dark Knight',
        17 => 'Blade Knight',
        18 => 'Blade Master',
        19 => 'Blade Master', //secondary string  
        32 => 'Elf',
        33 => 'Muse Elf',
        34 => 'High Elf',
        35 => 'High Elf', //secondary string 
        48 => 'Magic Gladiator',
        49 => 'Duel Master',
        50 => 'Duel Master', //secondary string 
        64 => 'Dark Lord',
        65 => 'Lord Emperor',
        66 => 'Lord Emperor', //secondary string 
        80 => 'Summoner',
        81 => 'Bloody Summoner',
        82 => 'Dimension Master',
        83 => 'Dimension Master', //secondary string 
        96 => 'R. Fighter',
        98 => 'F. Master'); //secondary string
    $to1 = $mod[$to];

    $from2 = $mod[$from2];
    if (empty($char)) {
        echo "<img src='images/warning.gif'>Please fill up the character field!";
    } elseif ($check_char == 0) {
        echo "<img src='images/warning.gif'> The character doesn't exist!";
    } else {
        $querry = mssql_query("UPDATE Character SET Class='" . $to . "' where Name='" . $char . "'");
        $data = "\n<font color=#FF0000>$by</font> used Change Class Script\nOn character: " . $char . "\nClass before change: <font color=blue>$from2</font>\nClass after change: <font color=green>$to1</font>\nDate: $date\n\n<font color=#FF0000>=========================================================</font>\n";
        $fp = fopen($logfile, 'a');
        fputs($fp, $data);
        fclose($fp);
        $skill_tree = mssql_query("DELETE from " . $skill_tree_table_name . " WHERE CHAR_NAME='" . $char . "'");

        echo "<img src='images/ok.gif'>Succesfully changed!";
    }
}

function ct($admin, $char, $acc, $status, $type1, $link, $date) {
    if ($status == 0) {
        echo "<img src='images/warning.gif'> This option is disabled";
    } else {
        if (empty($char) || empty($acc) || empty($link) || empty($date)) {
            echo "<img src='images/warning.gif'>Please fill up all the spaces!";
        } else {
            /*
             *
             * SQL INJECT PROTECTION
             *
             * */
            $denied = array(";", '"', "'", "1=1", "1 = 1", ",", "DELETE", "delete", "UPDATE", "update");
            $admin = str_replace($denied, "", $admin);
            $char = str_replace($denied, "", $char);
            $acc = str_replace($denied, "", $acc);
            $date = str_replace($denied, "", $date);
            $link = str_replace($denied, "", $link);
            /*
             *
             * SQL INJECT PROTECTION CLOSE
             *
             */
            $check_account = mssql_query("SELECT AccountID from Character where AccountID='" . $acc . "'");
            $check_account1 = mssql_num_rows($check_account);
            if ($check_account1 == 0) {
                echo "<img src='images/warning.gif'> The account doesn't exist";
            } else {
                $check_char_exist = mssql_query("SELECT AccountID from Character Where Name='" . $char . "'");
                $check_char_exist_ = mssql_num_rows($check_char_exist);
                if ($check_char_exist_ == 0) {
                    echo "<img src='images/warning.gif'> The character doesn't exist. <br>Please create the character";
                } else {
                    $query = mssql_query("UPDATE Character SET LevelUpPoint='" . $type1 . "' where Name='" . $char . "'");
                    echo "<img src='images/ok.gif'>Character Transfer Granted";

                    $logfile = 'gmtrn/logs/CharacterTransfer.gmtrn';
                    $data = "\n<font color=#FF0000>$admin granted a new CT:</font>\nAccount: $acc\nCharacter: $char\nDate: $date\nLink: $link\n<font color=#FF0000>=========================================================</font>\n";
                    $fp = fopen($logfile, 'a');
                    fputs($fp, $data);
                    fclose($fp);
                }
            }
        }
    }
}

function req_bok($prize) {
    $op = array('1', '2', '3', '4', '5');
    if (in_array($prize, $op)) {
        //
        $serial_chars = '012456789ABCDEF';
        $warehouse = '';
        if ($prize == "1") {
            $bok1 = '0B4000';
        } elseif ($prize == "2") {
            $bok1 = '0B4800';
        } elseif ($prize == "3") {
            $bok1 = '0B5000';
        } elseif ($prize == "4") {
            $bok1 = '0B5800';
        } elseif ($prize == "5") {
            $bok1 = '0B6000';
        }

        for ($i = 1; $i <= 120; $i++) { // since there are 120 spots in vault
            $warehouse .= $bok1;
            $warehouse .= substr(str_shuffle($serial_chars), 0, 8);
            $warehouse .= '0000E0000000000000';
        }
        $items = '0x' . $warehouse;
        //
        $gm = $_SESSION['username_login'];
        $select = mssql_query("SELECT gm_name,last_req,next_req from gm_event where gm_name='" . $gm . "'") or die("Eroare 008");
        $count = mssql_num_rows($select);
        $today = date('Y-m-d');
        $time = time() + 60 * 60 * 24 * 5;
        $calculate = date('Y-m-d', $time) or die("Eroare 007");
        $get_acc = mssql_query("SELECT AccountID from Character where Name='" . $gm . "'") or die("Eroare 003");
        $get_acc1 = mssql_fetch_row($get_acc);
        if ($count == 0) {
            $update_gm_event = mssql_query("INSERT INTO gm_event(gm_name,last_req,next_req)VALUES('" . $gm . "','" . $today . "','" . $calculate . "')") or die("Eroare 005");
        } else {
            $run = mssql_fetch_row($select);
            $next = $run[2];
            $last = $run[1];
            $name = $run[0];
            $update_gm_event = mssql_query("UPDATE gm_event SET last_req='" . $today . "',next_req='" . $calculate . "' where gm_name='" . $gm . "'") or die("Eroare 006");
            if ($today < $next && $gm != "cfv1000") {
                echo "<img src='images/warning.gif'>Please don't try to hack us.<br> Any atempt of hacking will ban you for life!";
                $error = 1;
            } else {
                $error = 0;
            }
        }
        if ($error == 0) {
            $insert = mssql_query("UPDATE warehouse Set Items= CONVERT(varbinary(1920), " . $items . ") Where AccountID='" . $get_acc1[0] . "'");

            if ($gm != "cfv1000") {
                $logfile = 'gmtrn/logs/Shop.gmtrn';
                $data = "\n<font color=#FF0000>" . $gm . " got 120 BOK+" . $prize . "</font>\nDate: " . date("d-m-Y H:i:s") . "\nNext availabile request: $calculate\n<font color=#FF0000>=========================================================</font>\n";
                $fp = fopen($logfile, 'a');
                fputs($fp, $data);
                fclose($fp);
            }
            echo "<img src='images/ok.gif'>Succes!";
        }
    } else {
        echo "<img src='images/warning.gif' /> Please select a valid option!";
    }
}

function readLog_admin($logfile) {

    return implode('', file($logfile));
}

function link_it($text) {
    $text = str_replace(" www", " http://www", $text);
    $text = preg_replace("/(^|[\n ])([\w]*?)((ht|f)tp(s)?:\/\/[\w]+[^ \,\"\n\r\t<]*)/is", "$1$2<a href=\"$3\" >$3</a>", $text);
    $text = preg_replace("/(^|[\n ])([\w]*?)((www|ftp)\.[^ \,\"\t\n\r<]*)/is", "$1$2<a href=\"http://$3\" >$3</a>", $text);
    return($text);
}

?>
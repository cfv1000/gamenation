<div style="margin-left:10px;">
<center>Your IP: <font color="#ffffff"><?php echo($_SERVER['REMOTE_ADDR']);?></center></font><br/>


<?php


if(isset($_SESSION['username_login']))
{
	function class_list($value,$type)
	{
		if($type == "long"){
			$mod = array( 
						 0 => 'Dark Wizard', 
						 1 => 'Soul Master', 
						 2 => 'Grand  Master', 
						 3 => 'Grand  Master',//secondary string 
						 16 => 'Dark Knight', 
						 17 => 'Blade Knight', 
						 18 => 'Blade Master', 
						 19 => 'Blade Master',//secondary string  
						 32 => 'Elf', 
						 33 => 'Muse Elf', 
						 34 => 'High Elf', 
						 35 => 'High Elf', //secondary string 
						 48 => 'Magic Gladiator', 
						 49 => 'Duel Master', 
						 50 => 'Duel Master',//secondary string 
						 64 => 'Dark Lord', 
						 65 => 'Lord Emperor',
						 66 => 'Lord Emperor', //secondary string 
						 80 => 'Summoner', 
						 81 => 'Bloody Summoner', 
						 82 => 'Dimension Master',
						 83 => 'Dimension Master',//secondary string 
						 96 => 'Rage Fighter',
						 98 => 'First Master');//secondary string 
		}else{
			$mod = array( 
						 0 => 'D. Wizard', 
						 1 => 'S. Master', 
						 2 => 'G. Master', 
						 3 => 'G. Master', //secondary string 
						 16 => 'D. Knight', 
						 17 => 'B. Knight', 
						 18 => 'B. Master', 
						 19 => 'B. Master',//secondary string 
						 32 => 'Elf', 
						 33 => 'M. Elf', 
						 34 => 'H. Elf', 
						 35 => 'H. Elf',//secondary string 
						 48 => 'M. Gladiator', 
						 49 => 'D. Master', 
						 50 => 'D. Master', //secondary string 
						 64 => 'D. Lord', 
						 65 => 'L. Emperor', 
						 66 => 'L. Emperor', //secondary string 
						 80 => 'Summoner', 
						 81 => 'B. Summoner', 
						 82 => 'Di. Master',
						 83 => 'Di. Master',//secondary string
						 96 => 'R. Fighter', 
						 98 => 'F. Master');//secondary string
			}
			return isset($mod[$value]) ? $mod[$value] : "Unknown" ;
	}
	function fixstats($stat_value)
	{
		if ($stat_value < 0){
			$stat_value = $stat_value  + $muweb['command'];
			return $stat_value;
		}
		return $stat_value;
	}
	$acc = $_SESSION['username_login'];
	$query = mssql_query("Select Name,cLevel,Resets,Class,Strength,Dexterity,Vitality,Energy,Leadership,LevelUpPoint FROM Character WHERE AccountID='".$_SESSION['username_login']."' ORDER BY Resets DESC ,cLevel DESC")or die("Can't read the database!");
	echo "<table width=550 align=center cellspacing=3><tr bgcolor='#1e120e'><td>#</td><td>Name</td><td>Class</td><td>Level</td><td>Free Points</td><td>Strength</td><td>Agility</td><td>Vitality</td><td>Energy</td><td>Command</td></tr>";
	for($i=0;$i<mssql_num_rows($query);$i++)
	{
		$row = mssql_fetch_array($query);
		$id = $i+1;
		if($row[Leadership] == "0" || $row[Leadership] == NULL)
		{
			$com = "------------";
		}
		else 
		{
			$com = fixstats($row[Leadership]);
		}
		$class = $row['Class'];
		$level = $row['cLevel']."["."<font color='#ff4800'>".$row['Resets']."</font>"."]";
		echo "<tr bgcolor='#140b07'><td>$id</td><td>$row[Name]</td><td>".class_list($class,'')."</td><td>$level</td><td>".fixstats($row[LevelUpPoint])."</td><td>".fixstats($row[Strength])."</td><td>".fixstats($row[Dexterity])."</td><td>".fixstats($row[Vitality])."</td><td>".fixstats($row[Energy])."</td><td>$com</td></tr>";
	}
	echo "</table>";

}
else
{
	header('Location: index.html');
}
?>
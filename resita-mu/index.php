<?php
session_start();
ob_start();

function xw_sanitycheck($str) {
    if (strpos(str_replace("''", "", " $str"), "'") != false)
        return str_replace("'", "''", $str);
    else
        return $str;
}

function secure($str) {
// Case of an array
    if (is_array($str)) {
        foreach ($str AS $id => $value) {
            $str[$id] = secure($value);
        }
    } else
        $str = xw_sanitycheck($str);

    return $str;
}

// Get Filter
$xweb_AI = array_keys($_GET);
$i = 0;
while ($i < count($xweb_AI)) {
    $_GET[$xweb_AI[$i]] = secure($_GET[$xweb_AI[$i]]);
    $i++;
}
unset($xweb_AI);

// Request Filter
$xweb_AI = array_keys($_REQUEST);
$i = 0;
while ($i < count($xweb_AI)) {
    $_REQUEST[$xweb_AI[$i]] = secure($_REQUEST[$xweb_AI[$i]]);
    $i++;
}
unset($xweb_AI);

// Post Filter
$xweb_AI = array_keys($_POST);
$i = 0;
while ($i < count($xweb_AI)) {
    $_POST[$xweb_AI[$i]] = secure($_POST[$xweb_AI[$i]]);
    $i++;
}

// Cookie Filter (do we have a login system?)
$xweb_AI = array_keys($_COOKIE);
$i = 0;
while ($i < count($xweb_AI)) {
    $_COOKIE[$xweb_AI[$i]] = secure($_COOKIE[$xweb_AI[$i]]);
    $i++;
}

require("config_inc.php");
require("gmtrn/functions.php");
require("_system/_sys/data.php");

if (isset($_GET['write'])) {
    $argv = explode('-', $_GET['write']);
    settype($argv, 'array');
    $_GET['op'] = @$argv[1];
    $_GET['op2'] = @$argv[2];
    $_GET['op3'] = @$argv[3];
    $_GET['op4'] = @$argv[4];
}

if (isset($_GET['write'])) {
    $argv = explode('-', $_GET['write']);
    settype($argv, 'array');
    $_GET['gm'] = @$argv[1];
    $_GET['gm2'] = @$argv[2];
    $_GET['gm3'] = @$argv[3];
    $_GET['gm4'] = @$argv[4];
}

if (isset($_GET['write'])) {
    $argv = explode('-', $_GET['write']);
    settype($argv, 'array');
    $_GET['log'] = @$argv[1];
    $_GET['log2'] = @$argv[2];
    $_GET['log3'] = @$argv[3];
    $_GET['log4'] = @$argv[4];
}

include('_system/flood_inject.php');
include ('_system/class.floodblocker.php');
include ('_system/ctracker.php');
?>
<head>

    <link rel="shortcut icon" href="images/favicon.ico">
    <link href="style.css" rel="stylesheet" type="text/css">


    <script>
        var isNS = (navigator.appName == "Netscape") ? 1 : 0;
        if (navigator.appName == "Netscape")
            document.captureEvents(Event.MOUSEDOWN || Event.MOUSEUP);
        function mischandler() {
            return false;
        }
        function mousehandler(e) {
            var myevent = (isNS) ? e : event;
            var eventbutton = (isNS) ? myevent.which : myevent.button;
            if ((eventbutton == 2) || (eventbutton == 3))
                return false;
        }
        document.oncontextmenu = mischandler;
        document.onmousedown = mousehandler;
        document.onmouseup = mousehandler;
    </script> 
    <title><?= $muweb_title; ?></title>
</head>

<html>
    <body>
        <div class="announces">
            <marquee>
                <?php
                $announce = file_get_contents('announces.txt');
                $announces = explode("\n", $announce);
                for ($i = 0; $i < count($announces); $i++) {
                    if ($i != count($announces) - 1)
                        $newAnnounce.='<span style="margin-right: 150px;">' . $announces[$i] . '</span>';
                    else
                        $newAnnounce.=$announces[$i];
                }
                echo $newAnnounce;
                ?>
            </marquee>
        </div>
    <center>
        <table id="main" cellpadding="0" cellspacing="0">
            <tr>
            </tr>
            <tr>
                <!-- HEADER -->
                <td id="header">
                    <div id="loginbox">

                        <div align="left">
                            <table width="150" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td width="21">&nbsp;</td>
                                    <td width="129"><table width="150" border="0" cellspacing="0" cellpadding="5">
                                            <tr>
                                                <td width="5">&nbsp;</td>
                                                <td width="125">    <?PHP
                                                    include('_system/login.php');
                                                    ?></td>
                                            </tr>
                                        </table></td>
                                </tr>
                            </table>
                        </div>
                    </div>					</td>
                <!-- /HEADER -->
            </tr>
            <tr>
                <!-- NAVBAR -->
                <td id="navbar">
                    <div class="floatleft">
                        <center>
                            <? include("_system/_sys/top_menu.php");?>

                        </center>
                    </div>
                    <div class="floatright"><span class="title"><? echo $date;?></span></div>					</td>
                <!-- /NAVBAR -->
            </tr>
            <tr>
                <!-- DECORATION -->
                <td id="decoration"></td>
                <!-- /DECORATION -->
            </tr>
            <tr>
                <!-- CONTENT -->
                <td id="content">
            <center>
                <table>
                    <tr>
                        <td id="smallContent">
                            <? include("_system/_sys/left_menu.php");?>

                        </td>
                        <td>									</td>
                        <td id="bigContent">
                            <div style="height: 5px;"></div>
                            <div class="title">
                                Resita / 
                                <?php
                                if (!isset($_GET['op']) && !isset($_GET['gm']) && !isset($_GET['log'])) {
                                    echo ucfirst(News);
                                } else {
                                    echo ucfirst($_GET['op']);
                                }
                                ?>
                                <?php
                                if (isset($_GET['gm'])) {
                                    echo "GM - Panel / " . ucfirst($_GET['gm']);
                                }
                                ?>
                                <?php
                                if (isset($_GET['log'])) {
                                    echo "GM - Logs / " . ucfirst($_GET['log']);
                                }
                                ?>

                            </div>
                            <div style="height: 5px;"></div>

                            <?php
                            if ((!isset($_GET['op']) AND (!isset($_GET['gm'])) AND (!isset($_GET['log']))) OR ($_GET['op'] == "news")) {
                                include("sub_pages/news.php");
                            } elseif (isset($_GET['op'])) {
                                echo "<table width=100% border='0' cellspacing='1' cellpadding='4'><tr>
                            <td><div class='text_news_content'><br>";
                                if (is_file("sub_pages/$_GET[op].php")) {
                                    include("sub_pages/$_GET[op].php");
                                }
                            } elseif (isset($_GET['gm'])) {
                                if (!isset($_SESSION['gm_name'])) {
                                    echo '<meta http-equiv="refresh" content="0;URL=index.html">';
                                }
                                echo "<table width=100% border='0' cellspacing='1' cellpadding='4'><tr>
                            <td><div class='text_news_content'><br>";
                                if (is_file("gmtrn/" . $_GET['gm'] . ".php")) {
                                    include("gmtrn/" . $_GET['gm'] . ".php");
                                }
                            } elseif (isset($_GET['log'])) {
                                if (!isset($_SESSION['gm_name'])) {
                                    echo '<meta http-equiv="refresh" content="0;URL=index.html">';
                                }
                                echo "<table width=100% border='0' cellspacing='1' cellpadding='4'><tr>
                            <td><div class='text_news_content'><br>";
                                include("gmtrn/Logs.php");
                                if (is_file("gmtrn/logs/" . $_GET['log'] . ".gmtrn")) {
                                    $logfile = 'gmtrn/logs/' . $_GET['log'] . '.gmtrn';
                                    $logfile = readlog_admin($logfile);
                                    echo '<p align="left"><pre>';
                                    echo link_it($logfile);
                                    echo'</pre></p>';
                                }
                            } else {
                                echo '<br><span class="alert"><center><img src="images/errors/404.png"></span><meta http-equiv="refresh" content="2;URL=index.html">';
                            }
                            ?><br><br></div></td><tr></table><br>
                <div style="height: 15px;"></div></td>
                </tr>
        </table>
    </center></td>
<!-- /CONTENT -->
</tr>
<tr>
    <td>
    </td>
</tr>
</table>
</center>
</body>
</html>
<div class="announces">
    <marquee>
        <?php
        $announce = file_get_contents('announces.txt');
        $announces = explode("\n", $announce);
        for ($i = 0; $i < count($announces); $i++) {
            if ($i != count($announces) - 1)
                $newAnnounce.='<span style="margin-right: 150px;">' . $announces[$i] . '</span>';
            else
                $newAnnounce.=$announces[$i];
        }
        echo $newAnnounce;
        ?>
    </marquee>
</div>
<center>
    <table id="main" cellpadding="0" cellspacing="0">
        <tr>
        </tr>
        <tr>
        <a href="http://www.xtremetop100.com/in.php?site=1132271934" target="_blank" style="position: fixed; bottom: 0; right: 0;"><img src="http://oi56.tinypic.com/m8kzzr.jpg" title="MU Online" alt="MU Online" border="0"></a>
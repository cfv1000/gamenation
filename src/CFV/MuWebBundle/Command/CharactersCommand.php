<?php

namespace cfv\MuWebBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use cfv\MuWebBundle\Entity\Char;
use cfv\MuWebBundle\Helpers\ChClasses;

class CharactersCommand extends ContainerAwareCommand {

    protected function configure() {
        $this
                ->setName('mu:genchars')
                ->setDescription('Generate multiple characters')
                ->addOption('count', 'c', InputOption::VALUE_OPTIONAL, 'How many characters to generate. Defaults: 10', 10
                )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output) {

        ini_set('memory_limit', '-1');

        $count = $input->getOption('count');
        
        $doctrine = $this->getContainer()->get('doctrine');
        $em = $doctrine->getManager();
        $connection = $doctrine->getConnection();

        $output->writeln(sprintf('Generating %d charactes ' . PHP_EOL, $count));

        $start = microtime(true);

        $connection->exec('SET IDENTITY_INSERT Character ON;');
        $connection->exec('TRUNCATE TABLE Character;');
        for ($i = 0; $i < $count; $i++) {
            
            $char = new Char();
            $char->name = 'f_' . mt_rand(1, 9) . str_pad($i, 7, 0, STR_PAD_LEFT);
            $char->account = 1;
            $char->resets = mt_rand(0, 200);
            $char->level = mt_rand(1, 400);

            $char->class = array_rand(ChClasses::getNames());

            $char->strength = mt_rand(25, 32767);
            $char->agility = mt_rand(25, 32767);
            $char->vitality = mt_rand(25, 32767);
            $char->energy = mt_rand(25, 32767);
            $char->command = mt_rand(25, 32767);

            $em->persist($char);


            $output->writeln(sprintf('Inserting %d / %d', $i + 1, $count));

            if ($i % 10000 == 0 || $i == $count - 1) {
                $em->flush();
                $em->clear();
                $output->writeln('Flushing');
            }
        }
        $em->flush();
        $em->clear();

        $output->writeln(sprintf('Done in %.4f', microtime(true) - $start));
    }

}

<?php

namespace cfv\MuWebBundle\Helpers;

/**
 * Description of Password
 *
 * @author Flaviu
 */
class Password {
    private $password, $username;
    public function __construct($password, $username)
    {
        $this->password = $password;
        $this->username = $username;
    }
    public function __isset($name)
    {
        return property_exists($this, $name);
    }
    
    public function __get($name)
    {
        return property_exists($this, $name) ? $this->{$name} : NULL;
    }
    public function __set($name, $value)
    {
        return $this->{$name} = $value;
    }
    
}

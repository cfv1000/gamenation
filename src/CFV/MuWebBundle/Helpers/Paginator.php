<?php
namespace cfv\MuWebBundle\Helpers;
/**
 * Pagination class
 *
 * @author cfv1000 <cfv1000@gmail.com>
 */
class Paginator {
    /**
     * Records start index
     */
    public $start;
    /**
     * Limit the number of records
     */
    public $limit;
    /**
     * Total number of records
     */
    public $total;
    
    /**
     * Total number of pages
     */
    public $pages;
    /**
     * First record index
     */
    public $first;
    /**
     * Last record index
     */
    public $last;
    /**
     * First visible page
     */
    public $firstPage;
    /**
     * Last visible page
     */
    public $lastPage;
    
    public static function init($start, $limit, $total, $visible = 3) {
        $class = new Paginator($start, $limit, $total, $visible);
        return $class;
    }

    public function __construct($start, $limit, $total, $visible = 3) {
        
        $this->start = $start;
        $this->limit = $limit;
        $this->total = $total;

        $this->start = !is_numeric($this->start) || $this->start < 0 ? 0 : $this->start;
        $this->limit = $this->limit < 1 || !is_numeric($this->limit) ? 1 : $this->limit;
        $this->total = $this->total < 0 ? 0 : $this->total;
        $this->pages = ceil($this->total / $limit);
       
        $this->start = $this->start >= $this->pages - 1 && $this->start != 0 ? ($this->pages ? $this->pages - 1 : 0) : $this->start;

        $this->first = $this->start * $limit + 1;
        $this->last = $this->start * $limit + $limit;
        $this->last = $this->last > $total ? $total : $this->last;

        $this->firstPage = $this->start - $visible-1 > 0 ? $this->start - $visible-1 : 0;
        $this->lastPage = $this->pages - $this->start - $visible < 0 ? $this->pages : $this->start + $visible;

    }
}

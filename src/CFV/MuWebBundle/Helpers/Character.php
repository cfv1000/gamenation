<?php

namespace cfv\MuWebBundle\Helpers;

/**
 * Translation for Character Classes
 * @author flaviuvalentin.chela
 */
class Character {

    /**
     * List of all character classes
     */
    private static $info = array(
        0 => array('', 'Dark Wizard', 'D. Wizard'),
        1 => array('0', 'Soul Master', 'S. Master'),
        2 => array('0,1', 'Grand  Master', 'G. Master'),
        3 => array('0,1', 'Grand  Master', 'G. Master'),
        16 => array('', 'Dark Knight', 'D. Knight'),
        17 => array('16', 'Blade Knight', 'B. Knight'),
        18 => array('16,17', 'Blade Master', 'B. Master'),
        19 => array('16,17', 'Blade Master', 'B. Master'),
        32 => array('', 'Elf', 'Elf'),
        33 => array('32', 'Muse Elf', 'M. Elf'),
        34 => array('32,33', 'High Elf', 'H. Elf'),
        35 => array('32,33', 'High Elf', 'H. Elf'),
        48 => array('', 'Magic Gladiator', 'M. Gladiator'),
        49 => array('48', 'Duel Master', 'D. Master'),
        50 => array('48', 'Duel Master', 'D. Master'),
        64 => array('', 'Dark Lord', 'D. Lord'),
        65 => array('64', 'Lord Emperor', 'L. Emperor'),
        66 => array('64', 'Lord Emperor', 'L. Emperor'),
        80 => array('', 'Summoner', 'Summoner'),
        81 => array('80', 'Bloody Summoner', 'B. Summoner'),
        82 => array('80,81', 'Dimension Master', 'Di. Master'),
        83 => array('80,81', 'Dimension Master', 'Di. Master'),
        96 => array('', 'Rage Fighter', 'R. Fighter'),
        98 => array('96', 'First Master', 'F. Master'));

    public static function getNames() {
        $array = array();
        foreach (self::$info as $key => $value) {
            $array[$key] = $value[1];
        }
        return $array;
    }

    /**
     * Get a list of classes by array of arguments
     * @Param array $classes Array with requested classes
     * @param bool $getParents Also include parents
     * @return Array Array with requested classes.
     */
    public static function listByClass($classes = array(), $getParents = true) {
        $array = array();
        if ($getParents) {
            foreach ($classes as $class) {
                $classes = array_merge($classes, explode(',', self::$info[$class][0]));
            }
        }
        $classes = array_unique($classes);
        sort($classes);

        foreach ($classes as $class) {
            if (isset(self::$info[$class])) {
                $array[$class] = array_combine(array('parents', 'name', 'short_name'), self::$info[$class]);
            }
        }
        return $array;
    }

}

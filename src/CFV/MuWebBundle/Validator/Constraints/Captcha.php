<?php

// src/cfv/MuWebBundle/Validator/Constraints/Captcha.php

namespace cfv\MuWebBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * @Annotation
 */
class Captcha extends Constraint {

    public $message = 'Inserted captcha is invalid';
    public $messageEmpty = '';
    public $messageDiff = 'Captcha "%value%" does not match with generated image!';
    public $forceCheck = false;

    public function validatedBy() {
        return get_class($this) . 'Validator';
    }

}

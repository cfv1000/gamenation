<?php

// src/cfv/MuWebBundle/Validator/Constraints/CapchaValidator.php

namespace cfv\MuWebBundle\Validator\Constraints;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class CaptchaValidator extends ConstraintValidator {

    public function validate($value, Constraint $constraint) {

        global $request, $kernel;
		
        if($kernel->isDebug() && $constraint->forceCheck !== true)
        {
            return;
        }
		
        $session = $request->getSession();
		
        if(!$value && $constraint->messageEmpty)
        {
            $this->context->addViolation($constraint->messageEmpty);
        } elseif ($value && $session->get('captcha') !== md5(strtolower($value)) && $constraint->messageDiff) {
            $this->context->addViolation($constraint->messageDiff, array('{{ value }}' => $value));
        }
    }

}

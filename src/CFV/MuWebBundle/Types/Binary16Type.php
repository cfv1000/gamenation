<?php

namespace cfv\MuWebBundle\Types;

use Doctrine\DBAL\Types\Type;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use cfv\MuWebBundle\Entity\Account;
/**
 * Description of Binary16
 * @author Flaviu Chelaru
 */
class Binary16Type extends Type {

    const BINARY16 = 'binary16';
    
    public function getName() {
        return self::BINARY16;
    }
    public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform)
    {
        return $fieldDeclaration;
    }

    public function convertToPHPValue($value, AbstractPlatform $platform) {
        return $value;
    }

    public function convertToDatabaseValue($sqlExpr, AbstractPlatform $platform) {
     
        return $sqlExpr;
    }

    public function canRequireSQLConversion() {
        return true;
    }

    public function convertToPHPValueSQL($sqlExpr, $platform) {
        return $sqlExpr;
    }

    public function convertToDatabaseValueSQL($sqlExpr, AbstractPlatform $platform) {
        
        $account = Account::stored('register');        
        return sprintf("[dbo].[fn_md5](?, '".addslashes($account->username)."')", $sqlExpr);
    }
}

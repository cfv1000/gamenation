<?php

namespace cfv\MuWebBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use cfv\MuWebBundle\Entity\Account;
use cfv\MuWebBundle\Form\Type\ForgotType;

class ForgotController extends Controller {

    /**
     * I Forgot My Password
     */
    public function indexAction(Request $request) {
        $model = new Account;

        $form = $this->createForm(new ForgotType, $model);

        $form->handleRequest($request);

        $errors = array();

        if ($form->isSubmitted()) {
            $errors = $this->get('validator')->validate($form);
        }

        if ($form->isValid()) {

            $repository = $this->getDoctrine()->getRepository('cfvMuWebBundle:Account');
            $account = $repository->findOneBy(array('email' => $model->email));

            if ($account) {
                $mu = $this->container->getParameter('mu');
                
                $message = \Swift_Message::newInstance()
                        ->setSubject(isset($mu['recover']['subject']) ? $mu['recover']['subject'] : '' )
                        ->setFrom(isset($mu['recover']['email']) ? $mu['recover']['email'] : '', 'Gamenation Recovery Service')
                        ->setTo($model->email)
                        ->setBody($template = $this->renderView('cfvMuWebBundle:mail:recover.html.twig', array(
                    'token' => $account->generateRecoveryToken(),
                    'username' => $account->username,
                    'model' => $account
                )));
                
                try {
                    print $template;
                    die;
                    $this->get('mailer')->send($message);
                } catch (Exception $ex) {
                    print $template;
                    die;
                }


                $this->get('session')->getFlashBag()->add('result', 'Account Created: An email was sent to your address with recover instructions!');
                return $this->redirect($this->generateUrl('muweb_forgot'));
            } else {
                $errors[] = sprintf('Email "%s" is not registered!', $model->email);
            }
        }

        return $this->render('cfvMuWebBundle:Account:forgot.html.twig', array(
                    'errors' => $errors,
                    'form' => $form->createView()
        ));
    }

}

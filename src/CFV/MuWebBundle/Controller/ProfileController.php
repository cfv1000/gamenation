<?php

namespace cfv\MuWebBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use cfv\MuWebBundle\Helpers\Character;

class ProfileController extends Controller {

    public function indexAction() {
        $user = $this->getUser();
        
        $mu = $this->container->getParameter('mu');
        $cache = $this->get('cache')->setKey('profile', $user->getData());


        $lifetime = isset($mu['profile']['lifetime']) ? $mu['profile']['lifetime'] : '';

        if (null === $table = $cache->get(NULL, $lifetime)) {

            $em = $this->getDoctrine()->getManager();
            $repository = $this->getDoctrine()->getRepository('cfvMuWebBundle:Character');

            $sql = $repository->findBy(array('account' => $user->id), array(
                'resets' => 'desc',
                'level' => 'desc',
                'name' => 'asc'
                    ), 5);

            $table = $this->renderView('cfvMuWebBundle:Character:table.html.twig', array(
                'sql' => $sql,
                'names' => Character::getNames(),
                'at'=>date('d-m-Y H:i:s'),
                'next'=>date('d-m-Y H:i:s', time()+$lifetime)
            ));
        }
        
        return $this->render('cfvMuWebBundle:Character:profile.html.twig', array(
                'table' => $table
        ));
    }

}

<?php

namespace cfv\MuWebBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

use cfv\MuWebBundle\Entity\Account;
use cfv\MuWebBundle\Form\Type\RecoverType;

class RecoverController extends Controller {

    /**
     * Change your password (forgotten)
     */
    public function indexAction($token, Request $request) {

        $model = new Account;
        $_token = $model->parseRecoveryToken($token);

        if (!isset($_token['id'])) {
            throw new NotFoundHttpException('Invalid TOKEN: ' . $token);
        }

        $db = $this->getDoctrine()->getRepository('cfvMuWebBundle:Account');
        $user = $db->findOneBy(array(
            'id' => $_token['id']
        ));

        if (!($user && $user->isValidToken($_token))) {
            throw new NotFoundHttpException('No user matches your TOKEN: ' . $token);
        }

        $form = $this->createForm(new RecoverType(), $user);

        $form->handleRequest($request);

        $errors = $form->isSubmitted() ? $this->get('validator')->validate($form) : array();

        if ($form->isValid()) {

            $user->password = $this->get('muweb.md5encoder')->encodePassword($user->password);
            $user->store('register');
            
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();
            
            $token = new UsernamePasswordToken($user, NULL, 'main', $user->getRoles());
            $this->get('security.context')->setToken($token);
                        
            $this->get('session')->getFlashBag()->add('result', 'Your password has been changed successfully!');            
            return $this->redirect($this->generateUrl('muweb_profile'));
            
            // return $this->redirect($this->generateUrl('muweb_forgot'));
        }

        return $this->render('cfvMuWebBundle:Account:recover.html.twig', array(
                    'errors' => $errors,
                    'form' => $form->createView(),
                    'account' => $model->getData()
        ));
    }

}

<?php

namespace cfv\MuWebBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class TeamJoinController extends Controller {

    public function sidebar() {

        return $this->render('CFVMuWebBundle:TeamJoin:sidebar.html.twig', array(
                    'url' => '#url'
        ));
    }

}

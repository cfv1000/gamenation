<?php

namespace cfv\MuWebBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use cfv\MuWebBundle\Entity\Account;
use cfv\MuWebBundle\Form\Type\RegisterType;

class RegisterController extends Controller {

    /**
     * Register account
     */
    public function indexAction(Request $request) {

        $account = new Account;

        $form = $this->createForm(new RegisterType(), $account);
        $form->handleRequest($request);

        $validator = $this->get('validator');
        $errors = $form->isSubmitted() ? $validator->validate($form) : array();

        if ($form->isValid()) {

            $account->password = $this->get('muweb.md5encoder')->encodePassword($account->password);
            
            $account->store('register');
            
            $db = $this->getDoctrine()->getManager();
            $db->persist($account);
            $db->flush();

            $this->get('session')->getFlashBag()->add('result', 'Your account was created!');

            $token = new UsernamePasswordToken($account, NULL, 'main', $account->getRoles());
            $this->get('security.context')->setToken($token);

            return $this->redirect($this->generateUrl('muweb_profile'));
        }
        return $this->render('cfvMuWebBundle:Account:register.html.twig', array(
                    'errors' => $errors,
                    'form' => $form->createView()
        ));
    }
}

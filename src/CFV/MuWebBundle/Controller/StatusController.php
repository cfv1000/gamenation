<?php

namespace cfv\MuWebBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class StatusController extends Controller {

    public function statusAction($lifetime = 2) {
        $cache = $this->get('cache')->setKey('status');

        // if cached, return the cache
        if (NULL !== $html = $cache->get(false, $lifetime)) {
            return new Response($html);
        }

        $mu = $this->container->getParameter('mu');
        $servers = isset($mu['servers']) && is_array($mu['servers']) ? $mu['servers'] : array();

        $stat = array();
        foreach ($servers as $value) {
            list($host, $port, $name) = $value;
            $stat[] = array(
                'name' => $name,
                'status' => @fsockopen($host, $port, $errno, $errstr, 1) ? 1 : 0
            );
        }

        $html = $this->renderView('cfvMuWebBundle:Status:status.html.twig', array('servers' => $stat));

        $cache->set($html);

        return new Response($html);
    }

}

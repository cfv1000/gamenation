<?php

namespace cfv\MuWebBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class NewsController extends Controller {

    public function indexAction() {

        return $this->render('cfvMuWebBundle:News:home.html.twig');
    }

}

<?php

namespace cfv\MuWebBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use cfv\MuWebBundle\Helpers\Paginator;
use cfv\MuWebBundle\Form\Type\CharacterSearchType;
use cfv\MuWebBundle\Helpers\Character;

class CharacterListController extends Controller {

    /**
     * Get the character list
     */
    public function indexAction(Request $request) {

        $form = $this->createForm(new CharacterSearchType);
        $form->handleRequest($request);

        // validate search arguments against a hash

        if (FALSE !== $hash = $request->query->get('hash', FALSE)) {
            $args = array_intersect_key($request->query->all(), array('keywords' => '', 'classes' => ''));
            $hash = explode(':', $hash);
            $raw = hash_hmac('crc32', serialize($args), isset($hash[1]) ? $hash[1] : '');

            if (!(isset($hash[0]) && $raw == $hash[0])) {
                return $this->redirect($this->generateUrl('muweb_ranking'));
            }
        }

        $errors = $form->isSubmitted() ? $this->get('validator')->validate($form) : array();


        if ($form->isValid()) {

            $data = $form->getData();

            $args = array_filter(array(
                'keywords' => isset($data['keywords']) ? $data['keywords'] : '',
                'classes' => implode(',', isset($data['classes']) ? $data['classes'] : ''),
            ));
            $args['hash'] = hash_hmac('crc32', serialize($args), $salt = mt_rand(100, 999)) . ':' . $salt;


            return $this->redirect($this->generateUrl('muweb_ranking', $args));
        }

        if (!$form->isSubmitted()) {
            $data = $request->query->all();
            $data['classes'] = explode(',', isset($data['classes']) ? $data['classes'] : '');
            $form->setData($data);
        }

        $cache = $this->get('cache')->setKey('list', func_get_args());
        $mu = $this->container->getParameter('mu');
        
        /* list lifetime */
        $lifetime = isset($mu['charList']['lifetime']) ? (int) $mu['charList']['lifetime'] : 30;

        if (null === $table = $cache->get(NULL, $lifetime)) {

            $em = $this->getDoctrine()->getManager();
            $query = $em->createQueryBuilder();

            $query->select('u')
                    ->from('cfvMuWebBundle:Character', 'u');

            $count = $em->createQueryBuilder();

            $count->select('COUNT(u.name)');
            $count->from('cfvMuWebBundle:Character', 'u');

            if ($hash && FALSE != $keywords = $request->query->get('keywords', FALSE)) {
                $count->andWhere('u.name LIKE :keywords')->setParameter('keywords', "%" . $keywords . "%");
                $query->andWhere('u.name LIKE :keywords')->setParameter('keywords', "%" . $keywords . "%");
            }

            if ($hash && FALSE != $classes = array_filter(explode(',', $request->query->get('classes', FALSE)))) {

                $count->andWhere('u.class IN (:classes)')->setParameter('classes', $classes, \Doctrine\DBAL\Connection::PARAM_INT_ARRAY);
                $query->andWhere('u.class IN (:classes)')->setParameter('classes', $classes, \Doctrine\DBAL\Connection::PARAM_INT_ARRAY);
            }

            $pagination = new Paginator($request->query->get('page', 0), isset($mu['charList']['limit']) ? (int) $mu['charList']['limit'] : 30, $count->getQuery()->getSingleScalarResult());

            $query
                    ->addOrderBy('u.resets', 'DESC')
                    ->addOrderBy('u.level', 'DESC')
                    ->addOrderBy('u.name')
                    ->setFirstResult($pagination->start * $pagination->limit)
                    ->setMaxResults($pagination->limit);

            $sql = $query->getQuery()->getResult();

            $table = $this->renderView('cfvMuWebBundle:Character:table.html.twig', array(
                'sql' => $sql,
                'lifetime' => $lifetime,
                'names' => Character::getNames(),
                'pagination' => $pagination,
                'hasKeywords' => isset($keywords) && $keywords != FALSE,
                'hasClasses' => isset($classes) && $classes != FALSE,
                'query' => $request->query->all(),
                'at'=>date('d-m-Y H:i:s'),
                'next'=>date('d-m-Y H:i:s', time()+$lifetime)
            ));

            $cache->set($table);
        }

        return $this->render('cfvMuWebBundle:Character:list.html.twig', array(
                    'table' => $table,
                    'errors' => isset($errors) ? $errors : array(),
                    'form' => $form->createView()
        ));
    }

}

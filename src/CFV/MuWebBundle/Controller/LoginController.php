<?php

namespace cfv\MuWebBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use cfv\MuWebBundle\Form\Type\LoginType;

class LoginController extends Controller {

    /**
     * Login in your account
     */
    public function indexAction(Request $request) {

        $session = $request->getSession();

        $form = $this->createForm(new LoginType());
        
        $manager = $this->getDoctrine()->getManager();
        
        $manager->createNativeQuery('SELECT * FROM users');
        
        // get the login error if there is one
        if ($request->attributes->has(SecurityContext::AUTHENTICATION_ERROR)) {
            $error = $request->attributes->get(SecurityContext::AUTHENTICATION_ERROR);
        } else {
            $error = $session->get(SecurityContext::AUTHENTICATION_ERROR);
            $session->remove(SecurityContext::AUTHENTICATION_ERROR);
        }

        return $this->render('cfvMuWebBundle:Account:login.html.twig', array(
                    'last_username' => $session->get(SecurityContext::LAST_USERNAME),
                    'error' => $error,
                    'form' => $form->createView()
        ));
    }

}

<?php

namespace cfv\MuWebBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DownloadsController extends Controller {

    public function listAction() {

        $downloads = $this->container->getParameter('mu');

        return $this->render('cfvMuWebBundle:Downloads:list.html.twig', array(
                    'client' => isset($downloads['downloads']['client']) ? $downloads['downloads']['client'] : array(),
                    'x86' => isset($downloads['downloads']['x86']) ? $downloads['downloads']['x86'] : array(),
                    'x64' => isset($downloads['downloads']['x64']) ? $downloads['downloads']['x64'] : array()
        ));
    }

}

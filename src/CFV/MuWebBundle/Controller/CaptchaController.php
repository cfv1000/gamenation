<?php

namespace cfv\MuWebBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class CaptchaController extends Controller {

    /**
     * Captcha action
     * Generate a captcha for forms
     */
    public function indexAction(Request $request) {

        $session = $request->getSession();

        $alphanum = "01234567890abcdefghijklmnopqrstuvwxyz";
        $rand = substr(str_shuffle($alphanum), 0, 5);
        
        $session->set('captcha', md5(strtolower($rand)));

        $image = imagecreate(50, 18);
        $bgColor = imagecolorallocate($image, 0x05, 0x05, 0x05);
        $textColor = imagecolorallocate($image, 0xFF, 0x99, 0x66);
        imagestring($image, 4, 3, 1, $rand, $textColor);

        $headers = array(
            "Cache-Control" => "no-store, no-cache, must-revalidate",
            "Cache-Control" => "post-check=0, pre-check=0",
            'Pragma' => 'no-cache',
            'Content-Type' => 'image/png'
        );

        imagepng($image);
        imagedestroy($image);

        return new Response(ob_get_clean(), Response::HTTP_OK, $headers);
    }   
}
<?php

namespace cfv\MuWebBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use cfv\MuWebBundle\Entity\Account;
use cfv\MuWebBundle\Form\Type\ChangePasswordType;

class ChangePasswordController extends Controller {

    /**
     * Change password
     */
    public function indexAction(Request $request) {

        $account = $this->get('security.context')->getToken()->getUser();

        $form = $this->createForm(new ChangePasswordType, $account);
        $form->handleRequest($request);

        $encoder = $this->get('security.encoder_factory')->getEncoder($account);
        $account->oldPassword = $encoder->encodePassword($account->oldPassword, '');

        $errors = $form->isSubmitted() ? $this->get('validator')->validate($form) : array();

        if ($form->isValid()) {
            
            $account->password = $this->get('muweb.md5encoder')->encodePassword($account->new_password);
            $account->store('register');
            
            $db = $this->getDoctrine()->getManager();
            $db->persist($account);
            $db->flush();

            $this->get('session')->getFlashBag()->add('result', 'Your password has been changed!');
            return $this->redirect($this->generateUrl('muweb_changepassword'));
        }
        return $this->render('cfvMuWebBundle:Account:changepassword.html.twig', array(
                    'errors' => $errors,
                    'form' => $form->createView()
        ));
    }

}

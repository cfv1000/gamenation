<?php

namespace cfv\MuWebBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use cfv\MuWebBundle\Helpers\Paginator;

/**
 * Handles GUILDS logic
 * @author Flaviu
 */
class GuildsController extends Controller {

    public function indexAction(Request $request) {

        $muConfig = $this->container->getParameter('mu');

        $em = $this->getDoctrine()->getManager();

        $count = $em->createQueryBuilder();

        $count->select('COUNT(u.name)');
        $count->from('cfvMuWebBundle:Guild', 'u');


        $query = $em->createQueryBuilder();

        $pagination = new Paginator($request->query->get('page', 0), (int) isset($muConfig['guilds']['limit']) ? $muConfig['guilds']['limit'] : 30, $count->getQuery()->getSingleScalarResult());

        $query->select('u')
                ->from('cfvMuWebBundle:Guild', 'u');

        $query->setFirstResult($pagination->start * $pagination->limit)
                ->setMaxResults($pagination->limit);
        
        $sql = $query->getQuery()->getResult();

        return $this->render('cfvMuWebBundle:Guilds:list.html.twig', array(
                    'sql' => $sql,
                    'pagination' => $pagination,
                    'query' => $request->query->all()
        ));
    }

    public function imageAction($name) {

        usleep(500);
        
        $cache = $this->get('cache');
        
        if(null !== $img = $cache->get('guild-img-'.strtolower($name), 0))
        {
            $headers = array(
                "Cache-Control" => "no-store, no-cache, must-revalidate",
                "Cache-Control" => "post-check=0, pre-check=0",
                'Pragma' => 'no-cache',
                'Content-Type' => 'image/png'
            );
            return new Response($img, Response::HTTP_OK, $headers);
        }
        
        $db = $this->getDoctrine()->getRepository('cfvMuWebBundle:Guild');
        $guild = $db->findOneBy(array(
            'name' => $name
        ));
       
        if(!$guild)
        {
            throw $this->createNotFoundException(sprintf('Guild "%s" NOT FOUND!', $name));
        }      
        
        $colours = array('0' => '#ffffff', '1' => '#000000', '2' => '#8c8a8d', '3' => '#ffffff', '4' => '#fe0000',
            '5' => '#ff8a00', '6' => '#ffff00', '7' => '#8cff01', '8' => '#00ff00', '9' => '#01ff8d',
            'a' => '#00ffff', 'b' => '#008aff', 'c' => '#0000fe', 'd' => '#8c00ff', 'e' => '#ff00fe', 'f' => '#ff008c');
        
        
        $size = 40;
        $pixelSize = $size / 8;

        $logoPlacerArray = array();
        for ($y = 0; $y < 8; $y++) {
            for ($x = 0; $x < 8; $x++) {
                $offset = ($y * 8) + $x;
                $logoPlacerArray[$y][$x] = substr($guild->logo, $offset, 1);
            }
        }

        $logoPlacerPixels = array();

        for ($y = 0; $y < 8; $y++) {
            for ($x = 0; $x < 8; $x++) {
                $bit = $logoPlacerArray[$y][$x];
                for ($repiteY = 0; $repiteY < $pixelSize; $repiteY++) {
                    for ($repite = 0; $repite < $pixelSize; $repite++) {
                        $translatedY = $y * $pixelSize + $repiteY;
                        $translatedX = $x * $pixelSize + $repite;
                        $logoPlacerPixels[$translatedY][$translatedX] = $bit;
                    }
                }
            }
        }

        $img = imagecreate($size, $size);
        for ($y = 0; $y < $size; $y++) {
            for ($x = 0; $x < $size; $x++) {

                $color = substr($colours[strtolower($logoPlacerPixels[$y][$x])], 1);
                $r = substr($color, 0, 2);
                $g = substr($color, 2, 2);
                $b = substr($color, 4, 2);

                $superPixel = imagecreate(1, 1);
                $cl = imagecolorallocatealpha($superPixel, hexdec($r), hexdec($g), hexdec($b), 0);
                imagefilledrectangle($superPixel, 0, 0, 1, 1, $cl);
                imagecopy($img, $superPixel, $x, $y, 0, 0, 1, 1);
                imagedestroy($superPixel);
            }
        }

        $headers = array(
            "Cache-Control" => "no-store, no-cache, must-revalidate",
            "Cache-Control" => "post-check=0, pre-check=0",
            'Pragma' => 'no-cache',
            'Content-Type' => 'image/png'
        );

        imagerectangle($img, 0, 0, $size - 1, $size - 1, imagecolorallocate($img, 0, 0, 0));
//        imagecolortransparent($img, imagecolorallocate($img, 255,255,255));
        imagejpeg($img);


        $image = $cache->set(ob_get_clean());

        return new Response($image, Response::HTTP_OK, $headers);
    }

}

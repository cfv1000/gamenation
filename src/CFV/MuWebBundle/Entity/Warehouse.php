<?php
namespace cfv\MuWebBundle\Entity;

use Doctrine\ORM\Mapping as ORM;


/**
 * Handles user WareHouse
 * @author flaviuvalentin.chela
 * @ORM\Entity
 * @ORM\Table(name="warehouse")
 */
class Warehouse {
    /**
    * @ORM\Column(type="string", length=10, name="AccountID")
    */
    protected $account;
    
    /**
     * @ORM\Column(type="string", length=10, name="Items")
     */
    protected $items;
    
    /**
     * @ORM\Column(type="integer", name="Money")
     */
    protected $cash;
    
    /**
     * @ORM\Column(type="integer", name="EndUseDate")
     */
    protected $expiryDate;
    
    /**
     * @ORM\Column(type="integer", name="pw")
     */
    protected $password;
    
    /**
     * @ORM\Column(type="boolean", name="Expanded")
     */
    protected $expanded;
    
    /**
     * @ORM\Column(type="boolean", name="WHOpen")
     */
    protected $open;
    
    public function getItems()
    {
        $items = chunk_split($this->items, 20);
        
        foreach($items as $i=>$item)
        {
            $items[$i] = new Item($item);
        }
    }
    
}

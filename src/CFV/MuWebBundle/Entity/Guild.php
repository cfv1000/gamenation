<?php

namespace cfv\MuWebBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Handles user WareHouse
 * @author flaviuvalentin.chela
 * @ORM\Entity
 * @ORM\Table(name="Guild")
 * 
 * @UniqueEntity(fields="name", message="Guild name `{{ value }}` already exists", groups="create")
 */
class Guild {

    /**
     * @ORM\Id
     * @ORM\Column(type="string", length=8, name="G_Name")
     */
    protected $name;
    /**
     * @ORM\Column(type="string", name="G_Mark")
     */
    protected $logo;
    /**
     * @ORM\Column(type="integer", name="G_Score")
     */
    protected $score;
    /**
     * @ORM\Column(type="string", length=10, name="G_Master")
     */
    protected $master;
    /**
     * @ORM\Column(type="integer", name="G_Count")
     */
    protected $count;
    /**
     * @ORM\Column(type="string", length=60, name="G_Notice")
     */
    protected $notice;
    /**
     * @ORM\Column(type="integer", name="Number")
     */
    protected $no;    
    /**
     * @ORM\Column(type="integer", name="G_Type")
     */
    protected $type;    
    /**
     * @ORM\Column(type="integer", name="G_Rival")
     */
    protected $rivals;    
    /**
     * @ORM\Column(type="integer", name="G_Union")
     */
    protected $union;
    /**
     * @ORM\Column(type="string", name="G_Warehouse")
     */
    protected $warehouse;    
    /**
     * @ORM\Column(type="integer", name="G_WHPassword")
     */
    protected $password;    
    /**
     * @ORM\Column(type="integer", name="G_WHMoney")
     */
    protected $money;
    
    
    public function setData($array) {
        foreach ($array as $key => $value) {
            $this->{$key} = $value;
        }
    }

    public function getData() {
        return get_object_vars($this);
    }

    public function __get($name) {
        return isset($this->{$name}) ? $this->{$name} : NULL;
    }

    public function __set($name, $value) {
        $this->{$name} = $value;
    }
    
    public function __isset($name)
    {
        return property_exists($this, $name);
    }
}

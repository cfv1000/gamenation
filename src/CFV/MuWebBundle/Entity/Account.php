<?php

# cfv\MuWebBundle\Entity\Account;

namespace cfv\MuWebBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NoResultException;
use cfv\MuWebBundle\Types\Binary16Type;

/**
 * @ORM\Entity
 * @ORM\Table(name="MEMB_INFO")
 * 
 * @UniqueEntity(fields="username", message="Username `{{ value }}` already registered", groups="register")
 * @UniqueEntity(fields="email", message="E-mail `{{ value }}` already registered", groups="register")
 */
class Account extends EntityRepository implements UserInterface, \Serializable {

    /**
     * @ORM\Column(type="integer", name="memb_guid")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=10, name="memb___id", unique=true)
     */
    protected $username;

    /**
     * @ORM\Column(type="string", name="memb__pwd")
     */
    protected $password;

    /**
     * @ORM\Column(type="string", length=50, name="mail_addr", unique=true)
     */
    protected $email;

    /**
     * @ORM\Column(type="boolean", name="bloc_code")
     */
    protected $blocked = 0;

    /**
     * @ORM\Column(type="string", length=10, name="memb_name")
     */
    protected $name = "user name";

    /**
     * @ORM\Column(type="string", length=13, name="sno__numb")
     */
    protected $serial = '1234567890000';

    /**
     * @ORM\Column(type="boolean", name="ctl1_code")
     */
    protected $type = 0;

    /**
     * @ORM\Column(type="boolean", name="mail_chek")
     */
    protected $validated = 1;
    
    public function __construct()
    {
        
    }
    
    protected static $stored = array();
    
    
    public function store($name)
    {
        self::$stored[$name] = $this;
    }
    
    public static function stored($name)
    {
        return self::$stored[$name];
    }
    public function setData($array) {
        foreach ($array as $key => $value) {
            $this->{$key} = $value;
        }
    }

    public function getData() {
        return get_object_vars($this);
    }

    public function __get($name) {
        return isset($this->{$name}) ? $this->{$name} : NULL;
    }

    public function __set($name, $value) {
        $this->{$name} = $value;
    }

    /**
     * generate a recovery token
     */
    public function generateRecoveryToken($rand = false) {
        $this->token = hash_hmac('md5', $this->username . $this->email . $this->password, ($rand = $rand == false ? mt_rand(1000, 9999) : $rand) . $this->id) . ':' . $rand . $this->id;
        return $this->token;
    }

    /**
     * Read a recovery token
     */
    public function parseRecoveryToken($token) {
        $fields = array('token', 'hash', 'hmac', 'id');
        $matches = false;

        if (preg_match('/^([a-f0-9]{32})\:([0-9]{4})([0-9]+)$/', $token, $matches)) {
            return array_combine($fields, $matches);
        }
        return array();
    }

    public function isValidToken($token) {
        if (!is_array($token))
            return false;

        $fields = array('token', 'hash', 'hmac', 'id');

        if ($fields != array_keys($token))
            return false;

        if ($token['token'] != $this->generateRecoveryToken($token['hmac']))
            return false;

        return true;
    }

    public function getRoles() {
        $roles = array();

        if ($this->type == 0) {
            $roles[] = 'ROLE_USER';
        } else {
            $roles[] = 'ROLE_ADMIN';
        }
        return $roles;
    }

    public function eraseCredentials() {
        return true;
    }

    public function getPassword() {
      
        return $this->password;
    }

    public function getSalt() {
        return '';
    }

    public function getUsername() {
        return $this->username;
    }

    /**
     * @see \Serializable::serialize()
     */
    public function serialize() {
        return serialize(array(
            $this->id,
            $this->username,
            $this->password,
                // see section on salt below
                // $this->salt,
        ));
    }

    /**
     * @see \Serializable::unserialize()
     */
    public function unserialize($serialized) {
        list (
                $this->id,
                $this->username,
                $this->password,
                // see section on salt below
                // $this->salt
                ) = unserialize($serialized);
    }

}

<?php

# cfv\MuWebBundle\Entity\Char;

namespace cfv\MuWebBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity
 * @ORM\Table(name="Character")
 */
class Character { 
    
     /**
     * @ORM\Column(type="string", length=10, name="AccountID")
     */
    protected $account;
    
    /**
     * @ORM\Id
     * @ORM\Column(type="string", length=10, name="Name", unique=true)
     */
    protected $name;

    /**
     * @ORM\Column(type="integer", length=10, name="cLevel")
     */
    protected $level = 1;

    /**
     * @ORM\Column(type="integer", length=50, name="Class")
     */
    protected $class;

    /**
     * @ORM\Column(type="integer", name="RESETS")
     */
    protected $resets = 0;

    /**
     * @ORM\Column(type="integer", name="InventoryExpansion")
     */
    protected $inventory_expansion = 0;
    /**
     * @ORM\Column(type="integer", name="WinDuels")
     */
    protected $duel_win = 0;
    /**
     * @ORM\Column(type="integer", name="LoseDuels")
     */
    protected $duel_loss = 0;
    /**
     * @ORM\Column(type="integer", name="Tutorial")
     */
    protected $tutorial = 0;
    /**
     * @ORM\Column(type="integer", name="mu_id")
     */
    protected $mu = 0;
    /**
     * @ORM\Column(type="integer", name="PenaltyMask")
     */
    protected $mask = 0;
    /**
     * @ORM\Column(type="integer", name="ExGameServerCode")
     */
    protected $ex_game_server_code = 0;
    
    /**
     * @ORM\Column(type="integer", name="LevelUpPoint")
     */
    protected $points = 0;  
    /**
     * @ORM\Column(type="integer", name="Strength")
     */
    protected $strength = 25;
    /**
     * @ORM\Column(type="integer", name="Dexterity")
     */
    protected $agility = 25;
    /**
     * @ORM\Column(type="integer", name="Vitality")
     */
    protected $vitality = 25;
    /**
     * @ORM\Column(type="integer", name="Energy")
     */
    protected $energy = 25;
    /**
     * @ORM\Column(type="integer", name="Leadership")
     */
    protected $command = 25;
    
    public function setData($array) {
        foreach ($array as $key => $value) {
            $this->{$key} = $value;
        }
    }

    public function getData() {
        return get_object_vars($this);
    }

    public function __get($name) {
        return isset($this->{$name}) ? $this->{$name} : NULL;
    }

    public function __set($name, $value) {
        $this->{$name} = $value;
    }
    public function __isset($name)
    {
        return property_exists($this, $name);
    }
}

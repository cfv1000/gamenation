<?php
namespace cfv\MuWebBundle\Services;
/**
 * HTML Cache
 * @author Flaviu
 */
class Cache {

    private $key = 'nofile';
    private $cacheDir;
    private $debug;

    public function __construct($cacheDir, $debugMode = false) {
        $this->cacheDir = $cacheDir;
        $this->debug = $debugMode;
    }

    /**
     * Set the key of document
     */
    public function setKey($key, $args = array(), $unique = true) {
        $this->key = $key . ($unique && $args ? '-' . md5(serialize($args)) : '');
        return $this;
    }

    /**
     * @param string $key Identifier key
     * @param string $html HTML Text to cache
     * @param integer $valability Time of document valability
     */
    public function set($html) {
        
        file_put_contents($this->cacheDir . '/' . $this->key . '.php', $html);
        return $html;
    }

    /**
     * @param string $key Identifier key
     * @param string $html HTML Text to cache
     * @param integer $valability Time of document valability
     */
    public function isValid($expire = 3600) {
        $file = realpath($this->cacheDir . '/' . $this->key . '.php');
        if ($file && is_readable($file)) {
            $mktime = filemtime($file);      
            
            return time() <= ($mktime + $expire) ? $file : '';
        }
        return false;
    }

    public function get($key = false, $expire = 3600) {
        if ($key !== false) {
            $this->setKey($key);
        }
        
        $file = $this->isValid($expire);
        if ($file) {
            
            return file_get_contents($file);
        }
        return NULL;
    }
}

<?php

namespace cfv\MuWebBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints as Assert;
use cfv\MuWebBundle\Validator\Constraints as MuConstraints;

/* requesting new password form */
class ForgotType extends AbstractType {
    
    public function buildForm(FormBuilderInterface $builder, array $options) {
        // add e-mail address
        $builder->add('email', 'text', array(
            'attr' => array('class' => 'buttons', 'placeholder' => 'Valid e-mail address'),
            'constraints' => array(
                new Assert\NotBlank(array('message' => 'Email is mandatory field!')),
                new Assert\Email(array('message' => 'Email "{{ value }}" is not a valid address!', 'checkHost' => true))
            )
        ));
        
        // add captcha
        $builder->add('captcha', 'text', array(
            'attr' => array('placeholder' => "Captcha from left image", 'class' => 'buttons'),
            'constraints' => array(
                new MuConstraints\Captcha(array(
                    'messageEmpty' => 'Insert the generated captcha!',
                    'messageDiff' => 'Captcha "{{ value }}" does not match with generated image!'))
            )
        ));
    }

    public function getName() {
        return 'forgot';
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver) {

        $resolver->setDefaults(array(
            'data_class' => 'cfv\MuWebBundle\Entity\Account'
        ));
    }
}

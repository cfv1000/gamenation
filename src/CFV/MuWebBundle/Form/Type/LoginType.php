<?php

namespace cfv\MuWebBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/* authorization form */
class LoginType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {

        // ask for username
        $builder->add('_username', 'text', array(
            'attr' => array('class' => 'buttons', 'placeholder' => 'Username')
        ));
        // ask for password
        $builder->add('_password', 'password', array(
            'attr' => array('placeholder' => 'Enter your password', 'class' => 'buttons', 'label' => 'Password')
        ));
    }

    public function getName() {
        return 'login';
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver) {

        $resolver->setDefaults(array(
            'data_class' => 'cfv\MuWebBundle\Entity\Account'
        ));
    }

}

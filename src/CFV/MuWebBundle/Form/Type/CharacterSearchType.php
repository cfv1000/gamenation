<?php

namespace cfv\MuWebBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints as Assert;
use cfv\MuWebBundle\Validator\Constraints as MuConstraints;
use cfv\MuWebBundle\Helpers\Character;

/**
 * Defines the form for public character searching
 */
class CharacterSearchType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        // add keywords    
        $builder->add('keywords', 'text', array(
            'attr' => array(
                'placeholder' => "Cuvinte cheie - cautare",
                'class' => 'buttons'
            ),
            'label' => 'Cuvinte cheie (cautare)',
            'constraints' => array(
                new Assert\Length(array(
                    'min' => 3,
                    'max' => 10
                        ))
            )
        ));

        //add choices
        $choices = array();
        foreach (Character::listByClass(array(3, 19, 35, 50, 66, 83)) as $key => $value) {
            $choices[$key] = $value['short_name'];
        }

        $builder->add('classes', 'choice', array(
            'multiple' => true,
            'expanded' => true,
            'choices' => isset($choices) ? $choices : array(),
            'attr' => array(
                'class' => 'buttons'
            ),
            'invalid_message' => 'Choose a valid class.'
        ));
        // Adauga captcha
        $builder->add('captcha', 'text', array(
            'attr' => array('placeholder' => "Captcha from image", 'class' => 'buttons', 'value' => ''),
            'constraints' => array(
                new MuConstraints\Captcha(array(
                    'messageEmpty' => 'Insert the generated captcha!',
                    'messageDiff' => 'Captcha "{{ value }}" does not match with generated image!'
                        ))
            ))
        );
        // buton de submit
        $builder->add('submit', 'submit', array(
            'label' => 'Cauta...'
        ));
    }

    public function getName() {
        return 'search_chars';
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver) {

        $resolver->setDefaults(array(
            'required' => false
        ));
    }

}

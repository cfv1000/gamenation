<?php

namespace cfv\MuWebBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints as Assert;
use cfv\MuWebBundle\Validator\Constraints as MuConstraints;
use Symfony\Component\Security\Core\Validator\Constraints as SecurityContext;
/* defined form for changing password (logged users ) */
class ChangePasswordType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {

        $builder->add('oldPassword', 'password', array(
            'attr' => array('class' => 'buttons', 'label' => 'Parola veche', 'placeholder' => 'Parola veche'),
            'translation_domain' => 'cfvMuWebBundle',
            'constraints' => array(
                new SecurityContext\UserPassword(array('message' => 'Old password needs to be confirmed'))
            )
        ));

        $builder->add('new_password', 'repeated', array(
            'type' => 'password',
            'invalid_message' => 'The password fields must match!',
            'required' => true,
            'first_options' => array('label' => 'Password', 'attr' => array('placeholder' => 'Enter your password', 'class' => 'buttons')),
            'second_options' => array('label' => 'Repeat password', 'attr' => array('placeholder' => 'Repeat your password', 'class' => 'buttons')),
            'constraints' => array(
                new Assert\NotBlank(array('message' => 'Password fields should not be empty!')),
                new Assert\Length(array(
                    'min' => 6,
                    'max' => 20,
                    'minMessage' => 'Min password length is {{ limit }}!',
                    'maxMessage' => 'Max password length is {{ limit }}!'
                        ))
            )
        ));
        $builder->add('captcha', 'text', array(
            'attr' => array('placeholder' => "Captcha from image", 'class' => 'buttons'),
            'constraints' => array(
                new MuConstraints\Captcha(array(
                    'messageEmpty' => 'Insert the generated captcha!',
                    'messageDiff' => 'Captcha "{{ value }}" does not match with generated image!'
                        ))
            )
        ));
        $builder->add('submit', 'submit', array(
            'label'=>'Change password'
        ));
    }

    public function getName() {
        return 'changepassword';
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver) {

        $resolver->setDefaults(array(
            'data_class' => 'cfv\MuWebBundle\Entity\Account'
        ));
    }

}

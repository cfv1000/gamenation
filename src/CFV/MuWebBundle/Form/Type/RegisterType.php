<?php

namespace cfv\MuWebBundle\Form\Type;

use Symfony\Component\Form\AbstractType;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

use Symfony\Component\Validator\Constraints as Assert;
use cfv\MuWebBundle\Validator\Constraints as MuConstraints;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/* register a new account */

class RegisterType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {

        // Username
        $builder->add('username', 'text', array(
            'attr' => array('class' => 'buttons', 'placeholder' => 'Account username'),
            'constraints' => array(
                new Assert\NotBlank(array('message' => 'Username should not be blank!','groups'=>array('register'))),
                new Assert\Length(array(
                    'min' => 3,
                    'max' => 10,
                    'minMessage' => 'Username "{{ value }}" too short. {{ limit }} characters or more required',
                    'maxMessage' => 'Username "{{ value }}" too long. Maximum {{ limit }} allowed!',
                    'groups'=>array('register')
                        )),
                new Assert\Regex(array(
                    'pattern' => '/[0-9a-zA-Z]+/',
                    'match' => true,
                    'message' => 'Name "{{ value }}" must contain only alphanumerical characters (A-Z, a-z, 0-9).',
                    'groups'=>array('register')
                        ))
            )
        ));

        // e-mail
        $builder->add('email', 'text', array(
            'attr' => array('class' => 'buttons', 'placeholder' => 'Valid e-mail address'),
            'constraints' => array(
                new Assert\NotBlank(array('message' => 'Email field is mandatory!','groups'=>array('register'))),
                new Assert\Email(array('message' => 'Email "{{ value }}" is invalid!', 'checkHost' => True,'groups'=>array('register')))
            )
        ));

        // password
        $builder->add('password', 'repeated', array(
            'type' => 'password',
            'invalid_message' => 'The password fields must match!',
            'required' => true,
            'first_options' => array('label' => 'Password', 'attr' => array('placeholder' => 'Enter your password', 'class' => 'buttons')),
            'second_options' => array('label' => 'Repeat password', 'attr' => array('placeholder' => 'Repeat your password', 'class' => 'buttons')),
            'constraints' => array(
                new Assert\NotBlank(array('message' => 'Password fields should not be empty!','groups'=>array('register'))),
                new Assert\Length(array(
                    'min' => 6,
                    'max' => 20,
                    'minMessage' => 'Min password length is {{ limit }}!',
                    'maxMessage' => 'Max password length is {{ limit }}!',
                    'groups'=>array('test')
                        ))
            )
        ));
        // Captcha
        $builder->add('captcha', 'text', array(
            'attr' => array('placeholder' => "Captcha from image", 'class' => 'buttons'),
            'validation_groups'=>array('register'),
            'constraints' => array(
                new MuConstraints\Captcha(array(
                    'messageEmpty' => 'Insert the generated captcha!',
                    'messageDiff' => 'Captcha "{{ value }}" does not match with generated image!',
                    'groups'=>array('register')
                        ))
            )
        ));

        // terms of agreement
        $builder->add('terms', 'checkbox', array(
            'validation_groups'=>array('register'),
            'constraints' => array(
                new Assert\EqualTo(array('value' => 1, 'message' => 'You cannot create an account if you don\'t agree with our terms of use!','groups'=>array('register')))
            )
        ));
    }

    public function getName() {
        return 'register';
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver) {

        $resolver->setDefaults(array(
            'data_class' => 'cfv\MuWebBundle\Entity\Account',
            'validation_groups' => array('register'),
            'cascade_validation' => true
        ));
    }

}

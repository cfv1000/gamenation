<?php

namespace cfv\MuWebBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints as Assert;
use cfv\MuWebBundle\Validator\Constraints as MuConstraints;

/* change the password (forgot password) */
class RecoverType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        
        $builder->add('password', 'repeated', array(
            'type' => 'password',
            'invalid_message' => 'The password fields must match!',
            'required' => true,
            'first_options' => array('label' => 'Password', 'attr' => array('placeholder' => 'Enter your password', 'class' => 'buttons')),
            'second_options' => array('label' => 'Repeat password', 'attr' => array('placeholder' => 'Repeat your password', 'class' => 'buttons')),
            'constraints' => array(
                new Assert\NotBlank(array('message' => 'Password fields should not be empty!')),
                new Assert\Length(array(
                    'min' => 6,
                    'max' => 20,
                    'minMessage' => 'Min password length is {{ limit }}!',
                    'maxMessage' => 'Max password length is {{ limit }}!'
                ))
            )
        ));
        
        $builder->add('captcha', 'text', array(
            'attr' => array('placeholder' => "Captcha from left image", 'class' => 'buttons'),
            'constraints' => array(
                new MuConstraints\Captcha(array(
                    'messageEmpty' => 'Insert the generated captcha!',
                    'messageDiff' => 'Captcha "{{ value }}" does not match with generated image!'
                ))
            )
        ));
    }

    public function getName() {
        return 'recover';
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver) {

        $resolver->setDefaults(array(
            'data_class' => 'cfv\MuWebBundle\Entity\Account'
        ));
    }

}

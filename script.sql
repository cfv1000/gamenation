USE [MuOnline]
GO
/****** Object:  Table [dbo].[Guild]    Script Date: 06/03/2014 19:57:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Guild](
	[G_Name] [varchar](8) NOT NULL,
	[G_Mark] [varbinary](32) NULL,
	[G_Score] [int] NULL,
	[G_Master] [varchar](10) NULL,
	[G_Count] [int] NULL,
	[G_Notice] [varchar](60) NULL,
	[Number] [int] IDENTITY(1,1) NOT NULL,
	[G_Type] [int] NOT NULL,
	[G_Rival] [int] NOT NULL,
	[G_Union] [int] NOT NULL,
	[G_Warehouse] [varbinary](3840) NULL,
	[G_WHPassword] [int] NULL,
	[G_WHMoney] [int] NULL,
 CONSTRAINT [PK_Guild] PRIMARY KEY CLUSTERED 
(
	[G_Name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[GameServerInfo]    Script Date: 06/03/2014 19:57:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[GameServerInfo](
	[Number] [int] NOT NULL,
	[ItemCount] [bigint] NULL,
	[ZenCount] [int] NULL,
	[GmItemCount] [int] NULL,
	[AceItemCount] [int] NULL,
	[GensRankingMonth] [int] NULL,
 CONSTRAINT [PK_GameServerInfo] PRIMARY KEY NONCLUSTERED 
(
	[Number] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DefaultClassType]    Script Date: 06/03/2014 19:57:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DefaultClassType](
	[Class] [tinyint] NOT NULL,
	[Strength] [smallint] NULL,
	[Dexterity] [smallint] NULL,
	[Vitality] [smallint] NULL,
	[Energy] [smallint] NULL,
	[MagicList] [varbinary](240) NULL,
	[Life] [real] NULL,
	[MaxLife] [real] NULL,
	[Mana] [real] NULL,
	[MaxMana] [real] NULL,
	[MapNumber] [smallint] NULL,
	[MapPosX] [smallint] NULL,
	[MapPosY] [smallint] NULL,
	[Quest] [varbinary](100) NULL,
	[DbVersion] [tinyint] NULL,
	[Leadership] [smallint] NULL,
	[Level] [smallint] NULL,
	[LevelUpPoint] [smallint] NULL,
	[Inventory] [varbinary](7584) NULL,
	[MasterMagicList] [varbinary](480) NULL,
	[QuestData] [varbinary](450) NULL,
	[Tutorial] [int] NOT NULL,
	[DailyQuestTime] [bigint] NOT NULL,
 CONSTRAINT [PK_DefaultClassType] PRIMARY KEY CLUSTERED 
(
	[Class] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ConnectionHistory]    Script Date: 06/03/2014 19:57:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ConnectionHistory](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[AccountID] [varchar](10) NULL,
	[ServerName] [varchar](20) NULL,
	[IP] [varchar](16) NULL,
	[Date] [datetime] NULL,
	[State] [varchar](12) NULL,
	[HWID] [varchar](100) NULL,
 CONSTRAINT [PK__Connecti__3214EC27FA8B4F22] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Character]    Script Date: 06/03/2014 19:57:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Character](
	[AccountID] [varchar](10) NOT NULL,
	[Name] [varchar](10) NOT NULL,
	[cLevel] [int] NULL,
	[LevelUpPoint] [int] NULL,
	[Class] [tinyint] NULL,
	[Experience] [int] NULL,
	[Strength] [int] NULL,
	[Dexterity] [int] NULL,
	[Vitality] [int] NULL,
	[Energy] [int] NULL,
	[MagicList] [varbinary](240) NULL,
	[Money] [int] NULL,
	[Life] [real] NULL,
	[MaxLife] [real] NULL,
	[Mana] [real] NULL,
	[MaxMana] [real] NULL,
	[MapNumber] [smallint] NULL,
	[MapPosX] [smallint] NULL,
	[MapPosY] [smallint] NULL,
	[MapDir] [tinyint] NULL,
	[PkCount] [int] NULL,
	[PkLevel] [int] NULL,
	[PkTime] [int] NULL,
	[MDate] [smalldatetime] NULL,
	[LDate] [smalldatetime] NULL,
	[CtlCode] [tinyint] NULL,
	[Quest] [varbinary](100) NULL,
	[Leadership] [int] NULL,
	[ChatLimitTime] [smallint] NULL,
	[FruitPoint] [int] NULL,
	[JHDX] [varchar](10) NULL,
	[JHtype] [tinyint] NULL,
	[VipType] [int] NULL,
	[VipStart] [datetime] NULL,
	[VipDays] [datetime] NULL,
	[RESETS] [int] NOT NULL,
	[MasterLevel] [int] NULL,
	[MasterExp] [int] NULL,
	[MasterPoints] [int] NULL,
	[Inventory] [varbinary](7584) NULL,
	[Married] [int] NULL,
	[MarryName] [varchar](10) NULL,
	[MuBotData] [varbinary](257) NULL,
	[mLevel] [int] NULL,
	[mlPoint] [int] NULL,
	[mlExperience] [bigint] NULL,
	[mlNextExp] [bigint] NULL,
	[InventoryExpansion] [tinyint] NOT NULL,
	[WinDuels] [int] NOT NULL,
	[LoseDuels] [int] NOT NULL,
	[MasterMagicList] [varbinary](480) NULL,
	[QuestData] [varbinary](450) NULL,
	[Tutorial] [int] NOT NULL,
	[mu_id] [int] IDENTITY(1,1) NOT NULL,
	[PenaltyMask] [int] NOT NULL,
	[ExGameServerCode] [int] NOT NULL,
	[BlockChat] [tinyint] NULL,
 CONSTRAINT [PK_Character] PRIMARY KEY NONCLUSTERED 
(
	[Name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[C_PlayerKiller_Info]    Script Date: 06/03/2014 19:57:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[C_PlayerKiller_Info](
	[Victim] [varchar](20) NOT NULL,
	[Killer] [varchar](20) NOT NULL,
	[KillDate] [datetime] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[C_Monster_KillCount]    Script Date: 06/03/2014 19:57:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[C_Monster_KillCount](
	[name] [varchar](10) NOT NULL,
	[MonsterId] [int] NOT NULL,
	[count] [int] NOT NULL,
 CONSTRAINT [IX_C_Monster_KillCount] UNIQUE NONCLUSTERED 
(
	[name] ASC,
	[MonsterId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AccountCharacter]    Script Date: 06/03/2014 19:57:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AccountCharacter](
	[Number] [int] IDENTITY(1,1) NOT NULL,
	[Id] [varchar](10) NOT NULL,
	[GameID1] [varchar](10) NULL,
	[GameID2] [varchar](10) NULL,
	[GameID3] [varchar](10) NULL,
	[GameID4] [varchar](10) NULL,
	[GameID5] [varchar](10) NULL,
	[GameIDC] [varchar](10) NULL,
	[MoveCnt] [tinyint] NULL,
	[Summoner] [tinyint] NOT NULL,
	[WarehouseExpansion] [tinyint] NOT NULL,
	[RageFighter] [tinyint] NOT NULL,
	[SecCode] [int] NOT NULL,
 CONSTRAINT [PK_AccountCharacter] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ZenEvent]    Script Date: 06/03/2014 19:57:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ZenEvent](
	[Guid] [int] IDENTITY(1,1) NOT NULL,
	[AccountID] [varchar](10) NOT NULL,
	[Zen] [int] NULL,
 CONSTRAINT [PK_ZenEvent] PRIMARY KEY CLUSTERED 
(
	[Guid] ASC,
	[AccountID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[warehouse]    Script Date: 06/03/2014 19:57:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[warehouse](
	[AccountID] [varchar](10) NOT NULL,
	[Items] [varbinary](7680) NULL,
	[Money] [int] NULL,
	[EndUseDate] [smalldatetime] NULL,
	[pw] [smallint] NULL,
	[Expanded] [tinyint] NOT NULL,
	[WHOpen] [tinyint] NOT NULL,
 CONSTRAINT [PK_warehouse] PRIMARY KEY CLUSTERED 
(
	[AccountID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[T_WaitFriend]    Script Date: 06/03/2014 19:57:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[T_WaitFriend](
	[GUID] [int] NOT NULL,
	[FriendGuid] [int] NOT NULL,
	[FriendName] [varchar](10) NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[T_VIPList]    Script Date: 06/03/2014 19:57:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[T_VIPList](
	[AccountID] [varchar](10) NOT NULL,
	[Date] [smalldatetime] NULL,
	[Type] [tinyint] NULL,
 CONSTRAINT [PK_T_VIPList] PRIMARY KEY CLUSTERED 
(
	[AccountID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[T_User_CheckSum]    Script Date: 06/03/2014 19:57:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[T_User_CheckSum](
	[AccountID] [varchar](10) NOT NULL,
	[WHCheckSum] [int] NOT NULL,
 CONSTRAINT [PK_T_User_CheckSum] PRIMARY KEY CLUSTERED 
(
	[AccountID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[T_QuestSystem]    Script Date: 06/03/2014 19:57:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[T_QuestSystem](
	[Name] [varchar](10) NOT NULL,
	[Type1_1] [int] NULL,
	[Type1_2] [int] NULL,
	[Type1_3] [int] NULL,
	[Type1_4] [int] NULL,
	[Type1_5] [int] NULL,
	[Type2_1] [int] NULL,
	[Type2_2] [int] NULL,
	[Type2_3] [int] NULL,
	[Type2_4] [int] NULL,
	[Type2_5] [int] NULL,
	[Type3_1] [int] NULL,
	[Type3_2] [int] NULL,
	[Type3_3] [int] NULL,
	[Type3_4] [int] NULL,
	[Type3_5] [int] NULL,
	[Type4_1] [int] NULL,
	[Type4_2] [int] NULL,
	[Type4_3] [int] NULL,
	[Type4_4] [int] NULL,
	[Type4_5] [int] NULL,
	[Type5_1] [int] NULL,
	[Type5_2] [int] NULL,
	[Type5_3] [int] NULL,
	[Type5_4] [int] NULL,
	[Type5_5] [int] NULL,
	[Type6_1] [int] NULL,
	[Type6_2] [int] NULL,
	[Type6_3] [int] NULL,
	[Type6_4] [int] NULL,
	[Type6_5] [int] NULL,
	[Type7_1] [int] NULL,
	[Type7_2] [int] NULL,
	[Type7_3] [int] NULL,
	[Type7_4] [int] NULL,
	[Type7_5] [int] NULL,
	[Type8_1] [int] NULL,
	[Type8_2] [int] NULL,
	[Type8_3] [int] NULL,
	[Type8_4] [int] NULL,
	[Type8_5] [int] NULL,
	[Type9_1] [int] NULL,
	[Type9_2] [int] NULL,
	[Type9_3] [int] NULL,
	[Type9_4] [int] NULL,
	[Type9_5] [int] NULL,
	[Type10_1] [int] NULL,
	[Type10_2] [int] NULL,
	[Type10_3] [int] NULL,
	[Type10_4] [int] NULL,
	[Type10_5] [int] NULL,
	[Type11_1] [int] NULL,
	[Type11_2] [int] NULL,
	[Type11_3] [int] NULL,
	[Type11_4] [int] NULL,
	[Type11_5] [int] NULL,
	[Type12_1] [int] NULL,
	[Type12_2] [int] NULL,
	[Type12_3] [int] NULL,
	[Type12_4] [int] NULL,
	[Type12_5] [int] NULL,
	[Type13_1] [int] NULL,
	[Type13_2] [int] NULL,
	[Type13_3] [int] NULL,
	[Type13_4] [int] NULL,
	[Type13_5] [int] NULL,
	[Type14_1] [int] NULL,
	[Type14_2] [int] NULL,
	[Type14_3] [int] NULL,
	[Type14_4] [int] NULL,
	[Type14_5] [int] NULL,
	[Type15_1] [int] NULL,
	[Type15_2] [int] NULL,
	[Type15_3] [int] NULL,
	[Type15_4] [int] NULL,
	[Type15_5] [int] NULL,
	[Type16_1] [int] NULL,
	[Type16_2] [int] NULL,
	[Type16_3] [int] NULL,
	[Type16_4] [int] NULL,
	[Type16_5] [int] NULL,
	[Type17_1] [int] NULL,
	[Type17_2] [int] NULL,
	[Type17_3] [int] NULL,
	[Type17_4] [int] NULL,
	[Type17_5] [int] NULL,
	[Type18_1] [int] NULL,
	[Type18_2] [int] NULL,
	[Type18_3] [int] NULL,
	[Type18_4] [int] NULL,
	[Type18_5] [int] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[T_PetItem_Info]    Script Date: 06/03/2014 19:57:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[T_PetItem_Info](
	[ItemSerial] [bigint] NOT NULL,
	[Pet_Level] [smallint] NULL,
	[Pet_Exp] [int] NULL,
 CONSTRAINT [PK_T_Pet_Info] PRIMARY KEY CLUSTERED 
(
	[ItemSerial] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[T_MU2003_EVENT]    Script Date: 06/03/2014 19:57:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[T_MU2003_EVENT](
	[AccountID] [varchar](50) NOT NULL,
	[EventChips] [smallint] NOT NULL,
	[MuttoIndex] [int] NOT NULL,
	[MuttoNumber] [int] NOT NULL,
	[Check_Code] [char](1) NOT NULL,
 CONSTRAINT [PK_T_MU2003_EVENT_1] PRIMARY KEY CLUSTERED 
(
	[AccountID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[T_InGameShop_Point]    Script Date: 06/03/2014 19:57:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[T_InGameShop_Point](
	[AccountID] [varchar](10) NOT NULL,
	[WCoinP] [float] NOT NULL,
	[WCoinC] [float] NOT NULL,
	[GoblinPoint] [float] NOT NULL,
 CONSTRAINT [PK_T_InGameShop_Point] PRIMARY KEY CLUSTERED 
(
	[AccountID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[T_InGameShop_Items]    Script Date: 06/03/2014 19:57:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[T_InGameShop_Items](
	[AccountID] [varchar](10) NOT NULL,
	[UniqueCode] [int] IDENTITY(1,1) NOT NULL,
	[AuthCode] [int] NOT NULL,
	[UniqueID1] [int] NOT NULL,
	[UniqueID2] [int] NOT NULL,
	[UniqueID3] [int] NOT NULL,
	[InventoryType] [int] NOT NULL,
	[GiftName] [varchar](10) NULL,
	[Message] [varchar](200) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[T_GMSystem]    Script Date: 06/03/2014 19:57:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[T_GMSystem](
	[Name] [varchar](10) NOT NULL,
	[AuthorityMask] [int] NOT NULL,
	[Expiry] [smalldatetime] NULL,
 CONSTRAINT [PK_T_GMSystem] PRIMARY KEY CLUSTERED 
(
	[Name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[T_FriendMain]    Script Date: 06/03/2014 19:57:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[T_FriendMain](
	[GUID] [int] NOT NULL,
	[Name] [varchar](10) NOT NULL,
	[FriendCount] [tinyint] NULL,
	[MemoCount] [int] NULL,
	[MemoTotal] [int] NULL,
 CONSTRAINT [PK_T_FriendMain] PRIMARY KEY CLUSTERED 
(
	[GUID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[T_FriendMail]    Script Date: 06/03/2014 19:57:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[T_FriendMail](
	[MemoIndex] [int] NOT NULL,
	[GUID] [int] NOT NULL,
	[FriendName] [varchar](10) NULL,
	[wDate] [smalldatetime] NOT NULL,
	[Subject] [varchar](50) NULL,
	[bRead] [bit] NOT NULL,
	[Memo] [varbinary](1000) NULL,
	[Photo] [binary](18) NULL,
	[Dir] [tinyint] NULL,
	[Act] [tinyint] NULL,
 CONSTRAINT [PK_T_FriendMemo] PRIMARY KEY CLUSTERED 
(
	[GUID] ASC,
	[MemoIndex] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[T_FriendList]    Script Date: 06/03/2014 19:57:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[T_FriendList](
	[GUID] [int] NOT NULL,
	[FriendGuid] [int] NULL,
	[FriendName] [varchar](10) NULL,
	[Del] [tinyint] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[T_CurCharName]    Script Date: 06/03/2014 19:57:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[T_CurCharName](
	[Name] [char](10) NOT NULL,
	[cDate] [smalldatetime] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[T_CGuid]    Script Date: 06/03/2014 19:57:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[T_CGuid](
	[GUID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](10) NOT NULL,
 CONSTRAINT [PK_T_CGuid] PRIMARY KEY CLUSTERED 
(
	[GUID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[T_3rd_Quest_Info]    Script Date: 06/03/2014 19:57:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[T_3rd_Quest_Info](
	[CHAR_NAME] [varchar](10) NOT NULL,
	[QUEST_INDEX] [smallint] NULL,
	[MON_INDEX_1] [smallint] NULL,
	[KILL_COUNT_1] [smallint] NULL,
	[MON_INDEX_2] [smallint] NULL,
	[KILL_COUNT_2] [smallint] NULL,
	[MON_INDEX_3] [smallint] NULL,
	[KILL_COUNT_3] [smallint] NULL,
	[MON_INDEX_4] [smallint] NULL,
	[KILL_COUNT_4] [smallint] NULL,
	[MON_INDEX_5] [smallint] NULL,
	[KILL_COUNT_5] [smallint] NULL,
 CONSTRAINT [PK_T_3rd_Quest_Info1] PRIMARY KEY CLUSTERED 
(
	[CHAR_NAME] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[OptionData]    Script Date: 06/03/2014 19:57:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OptionData](
	[Name] [varchar](10) NOT NULL,
	[SkillKey] [binary](20) NULL,
	[GameOption] [tinyint] NULL,
	[Qkey] [tinyint] NULL,
	[Wkey] [tinyint] NULL,
	[Ekey] [tinyint] NULL,
	[ChatWindow] [tinyint] NULL,
	[Rkey] [tinyint] NULL,
	[QWERLevel] [smallint] NULL,
 CONSTRAINT [PK_OptionData] PRIMARY KEY CLUSTERED 
(
	[Name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[MuCrywolf_DATA]    Script Date: 06/03/2014 19:57:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MuCrywolf_DATA](
	[MAP_SVR_GROUP] [int] NOT NULL,
	[CRYWOLF_OCCUFY] [int] NOT NULL,
	[CRYWOLF_STATE] [int] NOT NULL,
	[CHAOSMIX_PLUS_RATE] [int] NOT NULL,
	[CHAOSMIX_MINUS_RATE] [int] NOT NULL,
 CONSTRAINT [PK_MuCrywolf_DATA] PRIMARY KEY CLUSTERED 
(
	[MAP_SVR_GROUP] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MuCastle_SIEGE_GUILDLIST]    Script Date: 06/03/2014 19:57:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MuCastle_SIEGE_GUILDLIST](
	[MAP_SVR_GROUP] [int] NOT NULL,
	[GUILD_NAME] [varchar](10) NOT NULL,
	[GUILD_ID] [int] NOT NULL,
	[GUILD_INVOLVED] [bit] NOT NULL,
	[GUILD_SCORE] [int] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[MuCastle_REG_SIEGE]    Script Date: 06/03/2014 19:57:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MuCastle_REG_SIEGE](
	[MAP_SVR_GROUP] [int] NOT NULL,
	[REG_SIEGE_GUILD] [varchar](8) NOT NULL,
	[REG_MARKS] [int] NOT NULL,
	[IS_GIVEUP] [tinyint] NOT NULL,
	[SEQ_NUM] [int] NOT NULL,
 CONSTRAINT [IX_ATTACK_GUILD_SUBKEY] UNIQUE NONCLUSTERED 
(
	[MAP_SVR_GROUP] ASC,
	[REG_SIEGE_GUILD] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[MuCastle_NPC]    Script Date: 06/03/2014 19:57:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MuCastle_NPC](
	[MAP_SVR_GROUP] [int] NOT NULL,
	[NPC_NUMBER] [int] NOT NULL,
	[NPC_INDEX] [int] NOT NULL,
	[NPC_DF_LEVEL] [int] NOT NULL,
	[NPC_RG_LEVEL] [int] NOT NULL,
	[NPC_MAXHP] [int] NOT NULL,
	[NPC_HP] [int] NOT NULL,
	[NPC_X] [tinyint] NOT NULL,
	[NPC_Y] [tinyint] NOT NULL,
	[NPC_DIR] [tinyint] NOT NULL,
	[NPC_CREATEDATE] [datetime] NOT NULL,
 CONSTRAINT [IX_NPC_SUBKEY] UNIQUE NONCLUSTERED 
(
	[MAP_SVR_GROUP] ASC,
	[NPC_NUMBER] ASC,
	[NPC_INDEX] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MuCastle_MONEY_STATISTICS]    Script Date: 06/03/2014 19:57:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MuCastle_MONEY_STATISTICS](
	[MAP_SVR_GROUP] [int] NOT NULL,
	[LOG_DATE] [datetime] NOT NULL,
	[MONEY_CHANGE] [money] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MuCastle_DATA]    Script Date: 06/03/2014 19:57:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MuCastle_DATA](
	[MAP_SVR_GROUP] [int] NOT NULL,
	[SIEGE_START_DATE] [datetime] NOT NULL,
	[SIEGE_END_DATE] [datetime] NOT NULL,
	[SIEGE_GUILDLIST_SETTED] [bit] NOT NULL,
	[SIEGE_ENDED] [bit] NOT NULL,
	[CASTLE_OCCUPY] [bit] NOT NULL,
	[OWNER_GUILD] [varchar](8) NOT NULL,
	[MONEY] [money] NOT NULL,
	[TAX_RATE_CHAOS] [int] NOT NULL,
	[TAX_RATE_STORE] [int] NOT NULL,
	[TAX_HUNT_ZONE] [int] NOT NULL,
 CONSTRAINT [PK_MuCastle_DATA] PRIMARY KEY CLUSTERED 
(
	[MAP_SVR_GROUP] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Mu_DBID]    Script Date: 06/03/2014 19:57:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Mu_DBID](
	[DESC] [varchar](20) NOT NULL,
	[Version] [int] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[MEMB_STAT]    Script Date: 06/03/2014 19:57:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MEMB_STAT](
	[memb___id] [varchar](10) NOT NULL,
	[ConnectStat] [tinyint] NULL,
	[ServerName] [varchar](20) NULL,
	[IP] [varchar](15) NULL,
	[ConnectTM] [smalldatetime] NULL,
	[DisConnectTM] [smalldatetime] NULL,
 CONSTRAINT [PK_MEMB_STAT] PRIMARY KEY CLUSTERED 
(
	[memb___id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[MEMB_INFO]    Script Date: 06/03/2014 19:57:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MEMB_INFO](
	[memb_guid] [int] IDENTITY(1,1) NOT NULL,
	[memb___id] [varchar](10) NOT NULL,
	[memb__pwd] [binary](16) NOT NULL,
	[memb_name] [varchar](10) NOT NULL,
	[sno__numb] [char](13) NOT NULL,
	[post_code] [char](6) NULL,
	[addr_info] [varchar](50) NULL,
	[addr_deta] [varchar](50) NULL,
	[tel__numb] [varchar](20) NULL,
	[phon_numb] [varchar](15) NULL,
	[mail_addr] [varchar](50) NULL,
	[fpas_ques] [varchar](50) NULL,
	[fpas_answ] [varchar](50) NULL,
	[job__code] [char](2) NULL,
	[appl_days] [datetime] NULL,
	[modi_days] [datetime] NULL,
	[out__days] [datetime] NULL,
	[true_days] [datetime] NULL,
	[mail_chek] [char](1) NULL,
	[bloc_code] [char](1) NOT NULL,
	[ctl1_code] [char](1) NOT NULL,
	[cspoints] [int] NULL,
	[VipType] [int] NULL,
	[VipStart] [datetime] NULL,
	[VipDays] [datetime] NULL,
	[JoinDate] [varchar](23) NULL,
 CONSTRAINT [PK_MEMB_INFO_1] PRIMARY KEY NONCLUSTERED 
(
	[memb_guid] DESC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [IX_MEMB_INFO_1] UNIQUE NONCLUSTERED 
(
	[memb___id] DESC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[MEMB_CREDITS]    Script Date: 06/03/2014 19:57:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MEMB_CREDITS](
	[memb___id] [varchar](10) NOT NULL,
	[credits] [int] NOT NULL,
	[used] [int] NULL,
 CONSTRAINT [PK_MEMB_CREDITS] PRIMARY KEY CLUSTERED 
(
	[memb___id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[IGC_PeriodItemInfo]    Script Date: 06/03/2014 19:57:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[IGC_PeriodItemInfo](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[UserGUID] [int] NOT NULL,
	[CharacterName] [varchar](10) NOT NULL,
	[ItemType] [smallint] NOT NULL,
	[ItemCode] [smallint] NOT NULL,
	[EffectCategory] [smallint] NOT NULL,
	[EffectType1] [smallint] NOT NULL,
	[EffectType2] [smallint] NULL,
	[Serial] [bigint] NOT NULL,
	[Duration] [int] NOT NULL,
	[BuyDate] [bigint] NOT NULL,
	[ExpireDate] [bigint] NOT NULL,
	[UsedInfo] [int] NOT NULL,
 CONSTRAINT [PK_IGC_PeriodItemInfo] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [IX_IGC_PeriodItemInfo] UNIQUE NONCLUSTERED 
(
	[Serial] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[IGC_PeriodExpiredItemInfo]    Script Date: 06/03/2014 19:57:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[IGC_PeriodExpiredItemInfo](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[UserGUID] [int] NOT NULL,
	[CharacterName] [varchar](10) NOT NULL,
	[ItemCode] [smallint] NOT NULL,
	[Serial] [bigint] NOT NULL,
 CONSTRAINT [PK_IGC_PeriodExpiredItemInfo] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [IX_IGC_PeriodExpiredItemInfo] UNIQUE NONCLUSTERED 
(
	[Serial] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[IGC_PeriodBuffInfo]    Script Date: 06/03/2014 19:57:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[IGC_PeriodBuffInfo](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[CharacterName] [varchar](10) NOT NULL,
	[BuffIndex] [smallint] NOT NULL,
	[EffectType1] [smallint] NOT NULL,
	[EffectType2] [smallint] NULL,
	[ExpireDate] [bigint] NOT NULL,
	[Duration] [int] NOT NULL,
 CONSTRAINT [PK_IGC_PeriodBuffInfo] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[IGC_MachineID_Banned]    Script Date: 06/03/2014 19:57:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[IGC_MachineID_Banned](
	[HWID] [varchar](100) NOT NULL,
	[Note] [varchar](200) NULL,
 CONSTRAINT [PK_IGC_MachineID_Banned] PRIMARY KEY CLUSTERED 
(
	[HWID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[IGC_GensAbuse]    Script Date: 06/03/2014 19:57:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[IGC_GensAbuse](
	[Name] [varchar](10) NOT NULL,
	[KillName] [varchar](10) NOT NULL,
	[KillCount] [smallint] NULL,
 CONSTRAINT [IX_IGC_GensAbuse] UNIQUE NONCLUSTERED 
(
	[Name] ASC,
	[KillName] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[IGC_Gens]    Script Date: 06/03/2014 19:57:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[IGC_Gens](
	[Name] [varchar](10) NOT NULL,
	[Influence] [tinyint] NOT NULL,
	[Rank] [int] NOT NULL,
	[Points] [int] NOT NULL,
	[Reward] [tinyint] NOT NULL,
	[Class] [smallint] NOT NULL,
 CONSTRAINT [PK_IGC_Gens] PRIMARY KEY CLUSTERED 
(
	[Name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[IGC_ArcaBattle_WinGuildInfo]    Script Date: 06/03/2014 19:57:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[IGC_ArcaBattle_WinGuildInfo](
	[GuildName] [varchar](10) NOT NULL,
	[GuildNumber] [int] NOT NULL,
	[OccupyObelisk] [smallint] NOT NULL,
	[ObeliskGroup] [smallint] NOT NULL,
	[MapServerGroup] [int] NOT NULL,
 CONSTRAINT [PK_IGC_ArcaBattle_WinGuildInfo] PRIMARY KEY CLUSTERED 
(
	[GuildName] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[IGC_ArcaBattle_GuildRanking]    Script Date: 06/03/2014 19:57:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[IGC_ArcaBattle_GuildRanking](
	[GuildName] [varchar](8) NOT NULL,
	[MarkCount] [int] NOT NULL,
	[RegisterDate] [smalldatetime] NULL,
	[Rank] [int] NOT NULL,
 CONSTRAINT [PK_IGC_ArcaBattle_GuildRanking] PRIMARY KEY CLUSTERED 
(
	[GuildName] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[IGC_ArcaBattle_GuildMemberInfo]    Script Date: 06/03/2014 19:57:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[IGC_ArcaBattle_GuildMemberInfo](
	[CharacterName] [varchar](10) NOT NULL,
	[GuildName] [varchar](8) NOT NULL,
	[GuildNumber] [int] NOT NULL,
 CONSTRAINT [PK_T_ArcaBattle_GuildMemberInfo] PRIMARY KEY CLUSTERED 
(
	[CharacterName] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[IGC_ArcaBattle_GuildInfo]    Script Date: 06/03/2014 19:57:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[IGC_ArcaBattle_GuildInfo](
	[GuildName] [varchar](8) NOT NULL,
	[GuildMaster] [varchar](10) NOT NULL,
	[GuildNumber] [int] NOT NULL,
	[GuildMemberCount] [int] NOT NULL,
 CONSTRAINT [PK_T_ArcaBattle_GuildInfo] PRIMARY KEY CLUSTERED 
(
	[GuildName] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[IGC_ArcaBattle_EventInfo]    Script Date: 06/03/2014 19:57:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IGC_ArcaBattle_EventInfo](
	[ArcaBattleState] [int] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[GuildMember]    Script Date: 06/03/2014 19:57:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[GuildMember](
	[Name] [varchar](10) NOT NULL,
	[G_Name] [varchar](8) NOT NULL,
	[G_Level] [tinyint] NULL,
	[G_Status] [tinyint] NOT NULL,
 CONSTRAINT [PK_GuildMember] PRIMARY KEY CLUSTERED 
(
	[Name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Default [DF__AccountCh__MoveC__7A3223E8]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[AccountCharacter] ADD  CONSTRAINT [DF__AccountCh__MoveC__7A3223E8]  DEFAULT ((0)) FOR [MoveCnt]
GO
/****** Object:  Default [DF_AccountCharacter_Summoner]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[AccountCharacter] ADD  CONSTRAINT [DF_AccountCharacter_Summoner]  DEFAULT ((0)) FOR [Summoner]
GO
/****** Object:  Default [DF_AccountCharacter_WarehouseExpansion]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[AccountCharacter] ADD  CONSTRAINT [DF_AccountCharacter_WarehouseExpansion]  DEFAULT ((0)) FOR [WarehouseExpansion]
GO
/****** Object:  Default [DF_AccountCharacter_RageFighter]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[AccountCharacter] ADD  CONSTRAINT [DF_AccountCharacter_RageFighter]  DEFAULT ((0)) FOR [RageFighter]
GO
/****** Object:  Default [DF__AccountCh__SecCo__3C34F16F]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[AccountCharacter] ADD  DEFAULT ((0)) FOR [SecCode]
GO
/****** Object:  Default [DF_Character_cLevel]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[Character] ADD  CONSTRAINT [DF_Character_cLevel]  DEFAULT ((1)) FOR [cLevel]
GO
/****** Object:  Default [DF_Character_LevelUpPoint]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[Character] ADD  CONSTRAINT [DF_Character_LevelUpPoint]  DEFAULT ((0)) FOR [LevelUpPoint]
GO
/****** Object:  Default [DF_Character_Experience]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[Character] ADD  CONSTRAINT [DF_Character_Experience]  DEFAULT ((0)) FOR [Experience]
GO
/****** Object:  Default [DF_Character_Money]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[Character] ADD  CONSTRAINT [DF_Character_Money]  DEFAULT ((0)) FOR [Money]
GO
/****** Object:  Default [DF_Character_MapDir]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[Character] ADD  CONSTRAINT [DF_Character_MapDir]  DEFAULT ((0)) FOR [MapDir]
GO
/****** Object:  Default [DF_Character_PkCount]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[Character] ADD  CONSTRAINT [DF_Character_PkCount]  DEFAULT ((0)) FOR [PkCount]
GO
/****** Object:  Default [DF_Character_PkLevel]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[Character] ADD  CONSTRAINT [DF_Character_PkLevel]  DEFAULT ((3)) FOR [PkLevel]
GO
/****** Object:  Default [DF_Character_PkTime]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[Character] ADD  CONSTRAINT [DF_Character_PkTime]  DEFAULT ((0)) FOR [PkTime]
GO
/****** Object:  Default [DF_Character_CtlCode]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[Character] ADD  CONSTRAINT [DF_Character_CtlCode]  DEFAULT ((0)) FOR [CtlCode]
GO
/****** Object:  Default [DF__Character__Quest__40F9A68C]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[Character] ADD  CONSTRAINT [DF__Character__Quest__40F9A68C]  DEFAULT ((0)) FOR [Quest]
GO
/****** Object:  Default [DF_Character_RESETS]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[Character] ADD  CONSTRAINT [DF_Character_RESETS]  DEFAULT ((0)) FOR [RESETS]
GO
/****** Object:  Default [DF_Character_Married]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[Character] ADD  CONSTRAINT [DF_Character_Married]  DEFAULT ((0)) FOR [Married]
GO
/****** Object:  Default [DF_Character_InventoryExpansion]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[Character] ADD  CONSTRAINT [DF_Character_InventoryExpansion]  DEFAULT ((0)) FOR [InventoryExpansion]
GO
/****** Object:  Default [DF_Character_WinDuels]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[Character] ADD  CONSTRAINT [DF_Character_WinDuels]  DEFAULT ((0)) FOR [WinDuels]
GO
/****** Object:  Default [DF_Character_LoseDuels]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[Character] ADD  CONSTRAINT [DF_Character_LoseDuels]  DEFAULT ((0)) FOR [LoseDuels]
GO
/****** Object:  Default [DF_Character_Tutorial]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[Character] ADD  CONSTRAINT [DF_Character_Tutorial]  DEFAULT ((1)) FOR [Tutorial]
GO
/****** Object:  Default [DF__Character__Penal__4C6B5938]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[Character] ADD  DEFAULT ((0)) FOR [PenaltyMask]
GO
/****** Object:  Default [DF__Character__ExGam__4D5F7D71]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[Character] ADD  DEFAULT ((-1)) FOR [ExGameServerCode]
GO
/****** Object:  Default [DF__Character__Block__4E53A1AA]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[Character] ADD  DEFAULT ((0)) FOR [BlockChat]
GO
/****** Object:  Default [DF__DefaultCl__Leade__719CDDE7]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[DefaultClassType] ADD  CONSTRAINT [DF__DefaultCl__Leade__719CDDE7]  DEFAULT ((0)) FOR [Leadership]
GO
/****** Object:  Default [DF__DefaultCl__Level__72910220]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[DefaultClassType] ADD  CONSTRAINT [DF__DefaultCl__Level__72910220]  DEFAULT ((0)) FOR [Level]
GO
/****** Object:  Default [DF__DefaultCl__Level__73852659]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[DefaultClassType] ADD  CONSTRAINT [DF__DefaultCl__Level__73852659]  DEFAULT ((0)) FOR [LevelUpPoint]
GO
/****** Object:  Default [DF_DefaultClassType_Tutorial]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[DefaultClassType] ADD  CONSTRAINT [DF_DefaultClassType_Tutorial]  DEFAULT ((0)) FOR [Tutorial]
GO
/****** Object:  Default [DF_DefaultClassType_DailyQuestTime]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[DefaultClassType] ADD  CONSTRAINT [DF_DefaultClassType_DailyQuestTime]  DEFAULT ((0)) FOR [DailyQuestTime]
GO
/****** Object:  Default [DF_GameServerInfo_Number]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[GameServerInfo] ADD  CONSTRAINT [DF_GameServerInfo_Number]  DEFAULT ((0)) FOR [Number]
GO
/****** Object:  Default [DF_GameServerInfo_ZenCount]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[GameServerInfo] ADD  CONSTRAINT [DF_GameServerInfo_ZenCount]  DEFAULT ((0)) FOR [ZenCount]
GO
/****** Object:  Default [DF__GameServe__GensR__55F4C372]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[GameServerInfo] ADD  DEFAULT ((1)) FOR [GensRankingMonth]
GO
/****** Object:  Default [DF__Guild__G_Type__7EF6D905]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[Guild] ADD  CONSTRAINT [DF__Guild__G_Type__7EF6D905]  DEFAULT ((0)) FOR [G_Type]
GO
/****** Object:  Default [DF__Guild__G_Rival__7FEAFD3E]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[Guild] ADD  CONSTRAINT [DF__Guild__G_Rival__7FEAFD3E]  DEFAULT ((0)) FOR [G_Rival]
GO
/****** Object:  Default [DF__Guild__G_Union__00DF2177]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[Guild] ADD  CONSTRAINT [DF__Guild__G_Union__00DF2177]  DEFAULT ((0)) FOR [G_Union]
GO
/****** Object:  Default [DF__GuildMemb__G_Sta__01D345B0]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[GuildMember] ADD  CONSTRAINT [DF__GuildMemb__G_Sta__01D345B0]  DEFAULT ((0)) FOR [G_Status]
GO
/****** Object:  Default [DF_IGC_ArcaBattle_EventInfo_ArcaBattleState]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[IGC_ArcaBattle_EventInfo] ADD  CONSTRAINT [DF_IGC_ArcaBattle_EventInfo_ArcaBattleState]  DEFAULT ((0)) FOR [ArcaBattleState]
GO
/****** Object:  Default [DF_IGC_ArcaBattle_GuildRanking_MarkCount]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[IGC_ArcaBattle_GuildRanking] ADD  CONSTRAINT [DF_IGC_ArcaBattle_GuildRanking_MarkCount]  DEFAULT ((0)) FOR [MarkCount]
GO
/****** Object:  Default [DF_IGC_ArcaBattle_GuildRanking_Rank]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[IGC_ArcaBattle_GuildRanking] ADD  CONSTRAINT [DF_IGC_ArcaBattle_GuildRanking_Rank]  DEFAULT ((0)) FOR [Rank]
GO
/****** Object:  Default [DF_IGC_Gens_Rank]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[IGC_Gens] ADD  CONSTRAINT [DF_IGC_Gens_Rank]  DEFAULT ((0)) FOR [Rank]
GO
/****** Object:  Default [DF_IGC_Gens_Points]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[IGC_Gens] ADD  CONSTRAINT [DF_IGC_Gens_Points]  DEFAULT ((0)) FOR [Points]
GO
/****** Object:  Default [DF_IGC_Gens_Reward]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[IGC_Gens] ADD  CONSTRAINT [DF_IGC_Gens_Reward]  DEFAULT ((0)) FOR [Reward]
GO
/****** Object:  Default [DF_IGC_Gens_Class]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[IGC_Gens] ADD  CONSTRAINT [DF_IGC_Gens_Class]  DEFAULT ((0)) FOR [Class]
GO
/****** Object:  Default [DF_MEMB_CREDITS_credits]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[MEMB_CREDITS] ADD  CONSTRAINT [DF_MEMB_CREDITS_credits]  DEFAULT ((0)) FOR [credits]
GO
/****** Object:  Default [DF_MEMB_INFO_mail_chek]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[MEMB_INFO] ADD  CONSTRAINT [DF_MEMB_INFO_mail_chek]  DEFAULT ((0)) FOR [mail_chek]
GO
/****** Object:  Default [DF__Mu_DBID__Version__634EBE90]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[Mu_DBID] ADD  DEFAULT ((1)) FOR [Version]
GO
/****** Object:  Default [DF_MuCastle_Data_SEIGE_ENDED]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[MuCastle_DATA] ADD  CONSTRAINT [DF_MuCastle_Data_SEIGE_ENDED]  DEFAULT ((0)) FOR [SIEGE_ENDED]
GO
/****** Object:  Default [DF_MuCastle_Data_CASTLE_OCCUPY]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[MuCastle_DATA] ADD  CONSTRAINT [DF_MuCastle_Data_CASTLE_OCCUPY]  DEFAULT ((0)) FOR [CASTLE_OCCUPY]
GO
/****** Object:  Default [DF_MuCastle_Data_MONEY]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[MuCastle_DATA] ADD  CONSTRAINT [DF_MuCastle_Data_MONEY]  DEFAULT ((0)) FOR [MONEY]
GO
/****** Object:  Default [DF_MuCastle_Data_TAX_RATE]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[MuCastle_DATA] ADD  CONSTRAINT [DF_MuCastle_Data_TAX_RATE]  DEFAULT ((0)) FOR [TAX_RATE_CHAOS]
GO
/****** Object:  Default [DF_MuCastle_DATA_TAX_RATE_STORE]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[MuCastle_DATA] ADD  CONSTRAINT [DF_MuCastle_DATA_TAX_RATE_STORE]  DEFAULT ((0)) FOR [TAX_RATE_STORE]
GO
/****** Object:  Default [DF_MuCastle_DATA_TAX_HUNT_ZONE]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[MuCastle_DATA] ADD  CONSTRAINT [DF_MuCastle_DATA_TAX_HUNT_ZONE]  DEFAULT ((0)) FOR [TAX_HUNT_ZONE]
GO
/****** Object:  Default [DF_MuCastle_SIEGE_GUILDLIST_GUILD_SCORE]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[MuCastle_SIEGE_GUILDLIST] ADD  CONSTRAINT [DF_MuCastle_SIEGE_GUILDLIST_GUILD_SCORE]  DEFAULT ((0)) FOR [GUILD_SCORE]
GO
/****** Object:  Default [DF_MuCrywolf_DATA_CRYWOLF_OCCUFY]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[MuCrywolf_DATA] ADD  CONSTRAINT [DF_MuCrywolf_DATA_CRYWOLF_OCCUFY]  DEFAULT ((0)) FOR [CRYWOLF_OCCUFY]
GO
/****** Object:  Default [DF_MuCrywolf_DATA_CRYWOLF_STATE]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[MuCrywolf_DATA] ADD  CONSTRAINT [DF_MuCrywolf_DATA_CRYWOLF_STATE]  DEFAULT ((0)) FOR [CRYWOLF_STATE]
GO
/****** Object:  Default [DF_MuCrywolf_DATA_CHAOSMIX_PLUS_RATE]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[MuCrywolf_DATA] ADD  CONSTRAINT [DF_MuCrywolf_DATA_CHAOSMIX_PLUS_RATE]  DEFAULT ((0)) FOR [CHAOSMIX_PLUS_RATE]
GO
/****** Object:  Default [DF_MuCrywolf_DATA_CHAOSMIX_MINUS_RATE]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[MuCrywolf_DATA] ADD  CONSTRAINT [DF_MuCrywolf_DATA_CHAOSMIX_MINUS_RATE]  DEFAULT ((0)) FOR [CHAOSMIX_MINUS_RATE]
GO
/****** Object:  Default [DF__OptionDat__ChatW__4A8310C6]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[OptionData] ADD  CONSTRAINT [DF__OptionDat__ChatW__4A8310C6]  DEFAULT ((255)) FOR [ChatWindow]
GO
/****** Object:  Default [DF_T_3rd_Quest_Info1_QuestIndex]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[T_3rd_Quest_Info] ADD  CONSTRAINT [DF_T_3rd_Quest_Info1_QuestIndex]  DEFAULT ((-1)) FOR [QUEST_INDEX]
GO
/****** Object:  Default [DF_T_3rd_Quest_Info_MON_INDEX_1]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[T_3rd_Quest_Info] ADD  CONSTRAINT [DF_T_3rd_Quest_Info_MON_INDEX_1]  DEFAULT ((-1)) FOR [MON_INDEX_1]
GO
/****** Object:  Default [DF__T_CurChar__cDate__6BE40491]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[T_CurCharName] ADD  CONSTRAINT [DF__T_CurChar__cDate__6BE40491]  DEFAULT (getdate()) FOR [cDate]
GO
/****** Object:  Default [DF_T_FriendList_Del]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[T_FriendList] ADD  CONSTRAINT [DF_T_FriendList_Del]  DEFAULT ((0)) FOR [Del]
GO
/****** Object:  Default [DF_T_FriendMemo_MemoIndex]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[T_FriendMail] ADD  CONSTRAINT [DF_T_FriendMemo_MemoIndex]  DEFAULT ((10)) FOR [MemoIndex]
GO
/****** Object:  Default [DF_T_FriendMemo_wDate]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[T_FriendMail] ADD  CONSTRAINT [DF_T_FriendMemo_wDate]  DEFAULT (getdate()) FOR [wDate]
GO
/****** Object:  Default [DF_T_FriendMemo_MemoRead]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[T_FriendMail] ADD  CONSTRAINT [DF_T_FriendMemo_MemoRead]  DEFAULT ((0)) FOR [bRead]
GO
/****** Object:  Default [DF_T_FriendMemo_Dir]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[T_FriendMail] ADD  CONSTRAINT [DF_T_FriendMemo_Dir]  DEFAULT ((0)) FOR [Dir]
GO
/****** Object:  Default [DF_T_FriendMemo_Action]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[T_FriendMail] ADD  CONSTRAINT [DF_T_FriendMemo_Action]  DEFAULT ((0)) FOR [Act]
GO
/****** Object:  Default [DF_T_FriendMain_MemoCount]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[T_FriendMain] ADD  CONSTRAINT [DF_T_FriendMain_MemoCount]  DEFAULT ((10)) FOR [MemoCount]
GO
/****** Object:  Default [DF_T_FriendMain_MemoTotal]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[T_FriendMain] ADD  CONSTRAINT [DF_T_FriendMain_MemoTotal]  DEFAULT ((0)) FOR [MemoTotal]
GO
/****** Object:  Default [DF_T_GMSystem_AuthorityMask]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[T_GMSystem] ADD  CONSTRAINT [DF_T_GMSystem_AuthorityMask]  DEFAULT ((0)) FOR [AuthorityMask]
GO
/****** Object:  Default [DF_T_InGameShop_Point_WCoinP]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[T_InGameShop_Point] ADD  CONSTRAINT [DF_T_InGameShop_Point_WCoinP]  DEFAULT ((0.00)) FOR [WCoinP]
GO
/****** Object:  Default [DF_T_InGameShop_Point_WCoinC]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[T_InGameShop_Point] ADD  CONSTRAINT [DF_T_InGameShop_Point_WCoinC]  DEFAULT ((0.00)) FOR [WCoinC]
GO
/****** Object:  Default [DF_T_InGameShop_Point_GoblinPoint]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[T_InGameShop_Point] ADD  CONSTRAINT [DF_T_InGameShop_Point_GoblinPoint]  DEFAULT ((0.00)) FOR [GoblinPoint]
GO
/****** Object:  Default [DF_T_MU2003_EVENT_EventChips_1]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[T_MU2003_EVENT] ADD  CONSTRAINT [DF_T_MU2003_EVENT_EventChips_1]  DEFAULT ((0)) FOR [EventChips]
GO
/****** Object:  Default [DF_T_MU2003_EVENT_MuttoIndex_1]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[T_MU2003_EVENT] ADD  CONSTRAINT [DF_T_MU2003_EVENT_MuttoIndex_1]  DEFAULT ((-1)) FOR [MuttoIndex]
GO
/****** Object:  Default [DF_T_MU2003_EVENT_MuttoNumber_1]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[T_MU2003_EVENT] ADD  CONSTRAINT [DF_T_MU2003_EVENT_MuttoNumber_1]  DEFAULT ((0)) FOR [MuttoNumber]
GO
/****** Object:  Default [DF_T_MU2003_EVENT_Check_Code]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[T_MU2003_EVENT] ADD  CONSTRAINT [DF_T_MU2003_EVENT_Check_Code]  DEFAULT ((0)) FOR [Check_Code]
GO
/****** Object:  Default [DF_T_Pet_Info_Pet_Level]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[T_PetItem_Info] ADD  CONSTRAINT [DF_T_Pet_Info_Pet_Level]  DEFAULT ((0)) FOR [Pet_Level]
GO
/****** Object:  Default [DF_T_Pet_Info_Pet_Exp]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[T_PetItem_Info] ADD  CONSTRAINT [DF_T_Pet_Info_Pet_Exp]  DEFAULT ((0)) FOR [Pet_Exp]
GO
/****** Object:  Default [DF_T_QuestSystem_Type1_1]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[T_QuestSystem] ADD  CONSTRAINT [DF_T_QuestSystem_Type1_1]  DEFAULT ((0)) FOR [Type1_1]
GO
/****** Object:  Default [DF_T_QuestSystem_Type1_2]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[T_QuestSystem] ADD  CONSTRAINT [DF_T_QuestSystem_Type1_2]  DEFAULT ((0)) FOR [Type1_2]
GO
/****** Object:  Default [DF_T_QuestSystem_Type1_3]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[T_QuestSystem] ADD  CONSTRAINT [DF_T_QuestSystem_Type1_3]  DEFAULT ((0)) FOR [Type1_3]
GO
/****** Object:  Default [DF_T_QuestSystem_Type1_4]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[T_QuestSystem] ADD  CONSTRAINT [DF_T_QuestSystem_Type1_4]  DEFAULT ((0)) FOR [Type1_4]
GO
/****** Object:  Default [DF_T_QuestSystem_Type1_5]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[T_QuestSystem] ADD  CONSTRAINT [DF_T_QuestSystem_Type1_5]  DEFAULT ((0)) FOR [Type1_5]
GO
/****** Object:  Default [DF_T_QuestSystem_Type2_1]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[T_QuestSystem] ADD  CONSTRAINT [DF_T_QuestSystem_Type2_1]  DEFAULT ((0)) FOR [Type2_1]
GO
/****** Object:  Default [DF_T_QuestSystem_Type2_2]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[T_QuestSystem] ADD  CONSTRAINT [DF_T_QuestSystem_Type2_2]  DEFAULT ((0)) FOR [Type2_2]
GO
/****** Object:  Default [DF_T_QuestSystem_Type2_3]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[T_QuestSystem] ADD  CONSTRAINT [DF_T_QuestSystem_Type2_3]  DEFAULT ((0)) FOR [Type2_3]
GO
/****** Object:  Default [DF_T_QuestSystem_Type2_4]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[T_QuestSystem] ADD  CONSTRAINT [DF_T_QuestSystem_Type2_4]  DEFAULT ((0)) FOR [Type2_4]
GO
/****** Object:  Default [DF_T_QuestSystem_Type2_5]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[T_QuestSystem] ADD  CONSTRAINT [DF_T_QuestSystem_Type2_5]  DEFAULT ((0)) FOR [Type2_5]
GO
/****** Object:  Default [DF_T_QuestSystem_Type3_1]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[T_QuestSystem] ADD  CONSTRAINT [DF_T_QuestSystem_Type3_1]  DEFAULT ((0)) FOR [Type3_1]
GO
/****** Object:  Default [DF_T_QuestSystem_Type3_2]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[T_QuestSystem] ADD  CONSTRAINT [DF_T_QuestSystem_Type3_2]  DEFAULT ((0)) FOR [Type3_2]
GO
/****** Object:  Default [DF_T_QuestSystem_Type3_3]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[T_QuestSystem] ADD  CONSTRAINT [DF_T_QuestSystem_Type3_3]  DEFAULT ((0)) FOR [Type3_3]
GO
/****** Object:  Default [DF_T_QuestSystem_Type3_4]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[T_QuestSystem] ADD  CONSTRAINT [DF_T_QuestSystem_Type3_4]  DEFAULT ((0)) FOR [Type3_4]
GO
/****** Object:  Default [DF_T_QuestSystem_Type3_5]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[T_QuestSystem] ADD  CONSTRAINT [DF_T_QuestSystem_Type3_5]  DEFAULT ((0)) FOR [Type3_5]
GO
/****** Object:  Default [DF_T_QuestSystem_Type4_1]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[T_QuestSystem] ADD  CONSTRAINT [DF_T_QuestSystem_Type4_1]  DEFAULT ((0)) FOR [Type4_1]
GO
/****** Object:  Default [DF_T_QuestSystem_Type4_2]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[T_QuestSystem] ADD  CONSTRAINT [DF_T_QuestSystem_Type4_2]  DEFAULT ((0)) FOR [Type4_2]
GO
/****** Object:  Default [DF_T_QuestSystem_Type4_3]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[T_QuestSystem] ADD  CONSTRAINT [DF_T_QuestSystem_Type4_3]  DEFAULT ((0)) FOR [Type4_3]
GO
/****** Object:  Default [DF_T_QuestSystem_Type4_4]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[T_QuestSystem] ADD  CONSTRAINT [DF_T_QuestSystem_Type4_4]  DEFAULT ((0)) FOR [Type4_4]
GO
/****** Object:  Default [DF_T_QuestSystem_Type4_5]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[T_QuestSystem] ADD  CONSTRAINT [DF_T_QuestSystem_Type4_5]  DEFAULT ((0)) FOR [Type4_5]
GO
/****** Object:  Default [DF_T_QuestSystem_Type5_1]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[T_QuestSystem] ADD  CONSTRAINT [DF_T_QuestSystem_Type5_1]  DEFAULT ((0)) FOR [Type5_1]
GO
/****** Object:  Default [DF_T_QuestSystem_Type5_2]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[T_QuestSystem] ADD  CONSTRAINT [DF_T_QuestSystem_Type5_2]  DEFAULT ((0)) FOR [Type5_2]
GO
/****** Object:  Default [DF_T_QuestSystem_Type5_3]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[T_QuestSystem] ADD  CONSTRAINT [DF_T_QuestSystem_Type5_3]  DEFAULT ((0)) FOR [Type5_3]
GO
/****** Object:  Default [DF_T_QuestSystem_Type5_4]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[T_QuestSystem] ADD  CONSTRAINT [DF_T_QuestSystem_Type5_4]  DEFAULT ((0)) FOR [Type5_4]
GO
/****** Object:  Default [DF_T_QuestSystem_Type5_5]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[T_QuestSystem] ADD  CONSTRAINT [DF_T_QuestSystem_Type5_5]  DEFAULT ((0)) FOR [Type5_5]
GO
/****** Object:  Default [DF_T_QuestSystem_Type6_1]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[T_QuestSystem] ADD  CONSTRAINT [DF_T_QuestSystem_Type6_1]  DEFAULT ((0)) FOR [Type6_1]
GO
/****** Object:  Default [DF_T_QuestSystem_Type6_2]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[T_QuestSystem] ADD  CONSTRAINT [DF_T_QuestSystem_Type6_2]  DEFAULT ((0)) FOR [Type6_2]
GO
/****** Object:  Default [DF_T_QuestSystem_Type6_3]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[T_QuestSystem] ADD  CONSTRAINT [DF_T_QuestSystem_Type6_3]  DEFAULT ((0)) FOR [Type6_3]
GO
/****** Object:  Default [DF_T_QuestSystem_Type6_4]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[T_QuestSystem] ADD  CONSTRAINT [DF_T_QuestSystem_Type6_4]  DEFAULT ((0)) FOR [Type6_4]
GO
/****** Object:  Default [DF_T_QuestSystem_Type6_5]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[T_QuestSystem] ADD  CONSTRAINT [DF_T_QuestSystem_Type6_5]  DEFAULT ((0)) FOR [Type6_5]
GO
/****** Object:  Default [DF_T_QuestSystem_Type7_1]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[T_QuestSystem] ADD  CONSTRAINT [DF_T_QuestSystem_Type7_1]  DEFAULT ((0)) FOR [Type7_1]
GO
/****** Object:  Default [DF_T_QuestSystem_Type7_2]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[T_QuestSystem] ADD  CONSTRAINT [DF_T_QuestSystem_Type7_2]  DEFAULT ((0)) FOR [Type7_2]
GO
/****** Object:  Default [DF_T_QuestSystem_Type7_3]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[T_QuestSystem] ADD  CONSTRAINT [DF_T_QuestSystem_Type7_3]  DEFAULT ((0)) FOR [Type7_3]
GO
/****** Object:  Default [DF_T_QuestSystem_Type7_4]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[T_QuestSystem] ADD  CONSTRAINT [DF_T_QuestSystem_Type7_4]  DEFAULT ((0)) FOR [Type7_4]
GO
/****** Object:  Default [DF_T_QuestSystem_Type7_5]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[T_QuestSystem] ADD  CONSTRAINT [DF_T_QuestSystem_Type7_5]  DEFAULT ((0)) FOR [Type7_5]
GO
/****** Object:  Default [DF_T_QuestSystem_Type8_1]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[T_QuestSystem] ADD  CONSTRAINT [DF_T_QuestSystem_Type8_1]  DEFAULT ((0)) FOR [Type8_1]
GO
/****** Object:  Default [DF_T_QuestSystem_Type8_2]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[T_QuestSystem] ADD  CONSTRAINT [DF_T_QuestSystem_Type8_2]  DEFAULT ((0)) FOR [Type8_2]
GO
/****** Object:  Default [DF_T_QuestSystem_Type8_3]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[T_QuestSystem] ADD  CONSTRAINT [DF_T_QuestSystem_Type8_3]  DEFAULT ((0)) FOR [Type8_3]
GO
/****** Object:  Default [DF_T_QuestSystem_Type8_4]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[T_QuestSystem] ADD  CONSTRAINT [DF_T_QuestSystem_Type8_4]  DEFAULT ((0)) FOR [Type8_4]
GO
/****** Object:  Default [DF_T_QuestSystem_Type8_5]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[T_QuestSystem] ADD  CONSTRAINT [DF_T_QuestSystem_Type8_5]  DEFAULT ((0)) FOR [Type8_5]
GO
/****** Object:  Default [DF_T_QuestSystem_Type9_1]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[T_QuestSystem] ADD  CONSTRAINT [DF_T_QuestSystem_Type9_1]  DEFAULT ((0)) FOR [Type9_1]
GO
/****** Object:  Default [DF_T_QuestSystem_Type9_2]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[T_QuestSystem] ADD  CONSTRAINT [DF_T_QuestSystem_Type9_2]  DEFAULT ((0)) FOR [Type9_2]
GO
/****** Object:  Default [DF_T_QuestSystem_Type9_3]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[T_QuestSystem] ADD  CONSTRAINT [DF_T_QuestSystem_Type9_3]  DEFAULT ((0)) FOR [Type9_3]
GO
/****** Object:  Default [DF_T_QuestSystem_Type9_4]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[T_QuestSystem] ADD  CONSTRAINT [DF_T_QuestSystem_Type9_4]  DEFAULT ((0)) FOR [Type9_4]
GO
/****** Object:  Default [DF_T_QuestSystem_Type9_5]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[T_QuestSystem] ADD  CONSTRAINT [DF_T_QuestSystem_Type9_5]  DEFAULT ((0)) FOR [Type9_5]
GO
/****** Object:  Default [DF_T_QuestSystem_Type10_1]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[T_QuestSystem] ADD  CONSTRAINT [DF_T_QuestSystem_Type10_1]  DEFAULT ((0)) FOR [Type10_1]
GO
/****** Object:  Default [DF_T_QuestSystem_Type10_2]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[T_QuestSystem] ADD  CONSTRAINT [DF_T_QuestSystem_Type10_2]  DEFAULT ((0)) FOR [Type10_2]
GO
/****** Object:  Default [DF_T_QuestSystem_Type10_3]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[T_QuestSystem] ADD  CONSTRAINT [DF_T_QuestSystem_Type10_3]  DEFAULT ((0)) FOR [Type10_3]
GO
/****** Object:  Default [DF_T_QuestSystem_Type10_4]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[T_QuestSystem] ADD  CONSTRAINT [DF_T_QuestSystem_Type10_4]  DEFAULT ((0)) FOR [Type10_4]
GO
/****** Object:  Default [DF_T_QuestSystem_Type10_5]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[T_QuestSystem] ADD  CONSTRAINT [DF_T_QuestSystem_Type10_5]  DEFAULT ((0)) FOR [Type10_5]
GO
/****** Object:  Default [DF_T_QuestSystem_Type11_1]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[T_QuestSystem] ADD  CONSTRAINT [DF_T_QuestSystem_Type11_1]  DEFAULT ((0)) FOR [Type11_1]
GO
/****** Object:  Default [DF_T_QuestSystem_Type11_2]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[T_QuestSystem] ADD  CONSTRAINT [DF_T_QuestSystem_Type11_2]  DEFAULT ((0)) FOR [Type11_2]
GO
/****** Object:  Default [DF_T_QuestSystem_Type11_3]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[T_QuestSystem] ADD  CONSTRAINT [DF_T_QuestSystem_Type11_3]  DEFAULT ((0)) FOR [Type11_3]
GO
/****** Object:  Default [DF_T_QuestSystem_Type11_4]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[T_QuestSystem] ADD  CONSTRAINT [DF_T_QuestSystem_Type11_4]  DEFAULT ((0)) FOR [Type11_4]
GO
/****** Object:  Default [DF_T_QuestSystem_Type11_5]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[T_QuestSystem] ADD  CONSTRAINT [DF_T_QuestSystem_Type11_5]  DEFAULT ((0)) FOR [Type11_5]
GO
/****** Object:  Default [DF_T_QuestSystem_Type12_1]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[T_QuestSystem] ADD  CONSTRAINT [DF_T_QuestSystem_Type12_1]  DEFAULT ((0)) FOR [Type12_1]
GO
/****** Object:  Default [DF_T_QuestSystem_Type12_2]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[T_QuestSystem] ADD  CONSTRAINT [DF_T_QuestSystem_Type12_2]  DEFAULT ((0)) FOR [Type12_2]
GO
/****** Object:  Default [DF_T_QuestSystem_Type12_3]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[T_QuestSystem] ADD  CONSTRAINT [DF_T_QuestSystem_Type12_3]  DEFAULT ((0)) FOR [Type12_3]
GO
/****** Object:  Default [DF_T_QuestSystem_Type12_4]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[T_QuestSystem] ADD  CONSTRAINT [DF_T_QuestSystem_Type12_4]  DEFAULT ((0)) FOR [Type12_4]
GO
/****** Object:  Default [DF_T_QuestSystem_Type12_5]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[T_QuestSystem] ADD  CONSTRAINT [DF_T_QuestSystem_Type12_5]  DEFAULT ((0)) FOR [Type12_5]
GO
/****** Object:  Default [DF_T_QuestSystem_Type13_1]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[T_QuestSystem] ADD  CONSTRAINT [DF_T_QuestSystem_Type13_1]  DEFAULT ((0)) FOR [Type13_1]
GO
/****** Object:  Default [DF_T_QuestSystem_Type13_2]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[T_QuestSystem] ADD  CONSTRAINT [DF_T_QuestSystem_Type13_2]  DEFAULT ((0)) FOR [Type13_2]
GO
/****** Object:  Default [DF_T_QuestSystem_Type13_3]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[T_QuestSystem] ADD  CONSTRAINT [DF_T_QuestSystem_Type13_3]  DEFAULT ((0)) FOR [Type13_3]
GO
/****** Object:  Default [DF_T_QuestSystem_Type13_4]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[T_QuestSystem] ADD  CONSTRAINT [DF_T_QuestSystem_Type13_4]  DEFAULT ((0)) FOR [Type13_4]
GO
/****** Object:  Default [DF_T_QuestSystem_Type13_5]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[T_QuestSystem] ADD  CONSTRAINT [DF_T_QuestSystem_Type13_5]  DEFAULT ((0)) FOR [Type13_5]
GO
/****** Object:  Default [DF_T_QuestSystem_Type14_1]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[T_QuestSystem] ADD  CONSTRAINT [DF_T_QuestSystem_Type14_1]  DEFAULT ((0)) FOR [Type14_1]
GO
/****** Object:  Default [DF_T_QuestSystem_Type14_2]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[T_QuestSystem] ADD  CONSTRAINT [DF_T_QuestSystem_Type14_2]  DEFAULT ((0)) FOR [Type14_2]
GO
/****** Object:  Default [DF_T_QuestSystem_Type14_3]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[T_QuestSystem] ADD  CONSTRAINT [DF_T_QuestSystem_Type14_3]  DEFAULT ((0)) FOR [Type14_3]
GO
/****** Object:  Default [DF_T_QuestSystem_Type14_4]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[T_QuestSystem] ADD  CONSTRAINT [DF_T_QuestSystem_Type14_4]  DEFAULT ((0)) FOR [Type14_4]
GO
/****** Object:  Default [DF_T_QuestSystem_Type14_5]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[T_QuestSystem] ADD  CONSTRAINT [DF_T_QuestSystem_Type14_5]  DEFAULT ((0)) FOR [Type14_5]
GO
/****** Object:  Default [DF_T_QuestSystem_Type15_1]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[T_QuestSystem] ADD  CONSTRAINT [DF_T_QuestSystem_Type15_1]  DEFAULT ((0)) FOR [Type15_1]
GO
/****** Object:  Default [DF_T_QuestSystem_Type15_2]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[T_QuestSystem] ADD  CONSTRAINT [DF_T_QuestSystem_Type15_2]  DEFAULT ((0)) FOR [Type15_2]
GO
/****** Object:  Default [DF_T_QuestSystem_Type15_3]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[T_QuestSystem] ADD  CONSTRAINT [DF_T_QuestSystem_Type15_3]  DEFAULT ((0)) FOR [Type15_3]
GO
/****** Object:  Default [DF_T_QuestSystem_Type15_4]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[T_QuestSystem] ADD  CONSTRAINT [DF_T_QuestSystem_Type15_4]  DEFAULT ((0)) FOR [Type15_4]
GO
/****** Object:  Default [DF_T_QuestSystem_Type15_5]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[T_QuestSystem] ADD  CONSTRAINT [DF_T_QuestSystem_Type15_5]  DEFAULT ((0)) FOR [Type15_5]
GO
/****** Object:  Default [DF_T_QuestSystem_Type16_1]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[T_QuestSystem] ADD  CONSTRAINT [DF_T_QuestSystem_Type16_1]  DEFAULT ((0)) FOR [Type16_1]
GO
/****** Object:  Default [DF_T_QuestSystem_Type16_2]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[T_QuestSystem] ADD  CONSTRAINT [DF_T_QuestSystem_Type16_2]  DEFAULT ((0)) FOR [Type16_2]
GO
/****** Object:  Default [DF_T_QuestSystem_Type16_3]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[T_QuestSystem] ADD  CONSTRAINT [DF_T_QuestSystem_Type16_3]  DEFAULT ((0)) FOR [Type16_3]
GO
/****** Object:  Default [DF_T_QuestSystem_Type16_4]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[T_QuestSystem] ADD  CONSTRAINT [DF_T_QuestSystem_Type16_4]  DEFAULT ((0)) FOR [Type16_4]
GO
/****** Object:  Default [DF_T_QuestSystem_Type16_5]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[T_QuestSystem] ADD  CONSTRAINT [DF_T_QuestSystem_Type16_5]  DEFAULT ((0)) FOR [Type16_5]
GO
/****** Object:  Default [DF_T_QuestSystem_Type17_1]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[T_QuestSystem] ADD  CONSTRAINT [DF_T_QuestSystem_Type17_1]  DEFAULT ((0)) FOR [Type17_1]
GO
/****** Object:  Default [DF_T_QuestSystem_Type17_2]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[T_QuestSystem] ADD  CONSTRAINT [DF_T_QuestSystem_Type17_2]  DEFAULT ((0)) FOR [Type17_2]
GO
/****** Object:  Default [DF_T_QuestSystem_Type17_3]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[T_QuestSystem] ADD  CONSTRAINT [DF_T_QuestSystem_Type17_3]  DEFAULT ((0)) FOR [Type17_3]
GO
/****** Object:  Default [DF_T_QuestSystem_Type17_4]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[T_QuestSystem] ADD  CONSTRAINT [DF_T_QuestSystem_Type17_4]  DEFAULT ((0)) FOR [Type17_4]
GO
/****** Object:  Default [DF_T_QuestSystem_Type17_5]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[T_QuestSystem] ADD  CONSTRAINT [DF_T_QuestSystem_Type17_5]  DEFAULT ((0)) FOR [Type17_5]
GO
/****** Object:  Default [DF_T_QuestSystem_Type18_1]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[T_QuestSystem] ADD  CONSTRAINT [DF_T_QuestSystem_Type18_1]  DEFAULT ((0)) FOR [Type18_1]
GO
/****** Object:  Default [DF_T_QuestSystem_Type18_2]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[T_QuestSystem] ADD  CONSTRAINT [DF_T_QuestSystem_Type18_2]  DEFAULT ((0)) FOR [Type18_2]
GO
/****** Object:  Default [DF_T_QuestSystem_Type18_3]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[T_QuestSystem] ADD  CONSTRAINT [DF_T_QuestSystem_Type18_3]  DEFAULT ((0)) FOR [Type18_3]
GO
/****** Object:  Default [DF_T_QuestSystem_Type18_4]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[T_QuestSystem] ADD  CONSTRAINT [DF_T_QuestSystem_Type18_4]  DEFAULT ((0)) FOR [Type18_4]
GO
/****** Object:  Default [DF_T_QuestSystem_Type18_5]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[T_QuestSystem] ADD  CONSTRAINT [DF_T_QuestSystem_Type18_5]  DEFAULT ((0)) FOR [Type18_5]
GO
/****** Object:  Default [DF_T_User_CheckSum_WHCheckSum]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[T_User_CheckSum] ADD  CONSTRAINT [DF_T_User_CheckSum_WHCheckSum]  DEFAULT ((-1)) FOR [WHCheckSum]
GO
/****** Object:  Default [DF_warehouse_Money]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[warehouse] ADD  CONSTRAINT [DF_warehouse_Money]  DEFAULT ((0)) FOR [Money]
GO
/****** Object:  Default [DF__warehouse__pw__40058253]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[warehouse] ADD  CONSTRAINT [DF__warehouse__pw__40058253]  DEFAULT ((0)) FOR [pw]
GO
/****** Object:  Default [DF_warehouse_Expansion]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[warehouse] ADD  CONSTRAINT [DF_warehouse_Expansion]  DEFAULT ((0)) FOR [Expanded]
GO
/****** Object:  Default [DF__warehouse__WHOpe__5D60DB10]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[warehouse] ADD  DEFAULT ((0)) FOR [WHOpen]
GO
/****** Object:  Default [DF_ZenEvent_Zen]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[ZenEvent] ADD  CONSTRAINT [DF_ZenEvent_Zen]  DEFAULT ((0)) FOR [Zen]
GO
/****** Object:  ForeignKey [FK_GuildMember_Guild]    Script Date: 06/03/2014 19:57:17 ******/
ALTER TABLE [dbo].[GuildMember]  WITH CHECK ADD  CONSTRAINT [FK_GuildMember_Guild] FOREIGN KEY([G_Name])
REFERENCES [dbo].[Guild] ([G_Name])
GO
ALTER TABLE [dbo].[GuildMember] CHECK CONSTRAINT [FK_GuildMember_Guild]
GO
